//Recreating Stack
function ping(){
  if(agent && !callSession[currentCounterparty]){
    console.clear();
    sipStack.stop();
    setTimeout(ping, 3600000);
  } else {
    setTimeout(ping, 3600000);
  }
}
//creating connection to server that is needed to create and accept calls
function createNewSipStack(server, publicid, password, listener){
  return new SIPml.Stack({
		realm: server,
		impi: publicid,
		impu: 'sip:' + publicid + '@' + server,
		password: password,
		display_name: publicid,
		enable_rtcweb_breaker: true,
		bandwidth: { audio:64 },
		ice_servers: [{url:'stun:stun.patlive.com:3478'}],
		outbound_proxy_url:'udp://callingdesk.com:5060',
	  websocket_proxy_url: 'wss://callingdesk.com:8089/ws',
		events_listener: listener
	});
}
//registering to server(needed to recive incoming calls and to be all the time connected to server)
function login(sipStack){
    const registerSession = sipStack.newSession('register', {
        events_listener: stackListeners.loginListener
    });
    registerSession.register();
}

//Starting new incoming call session and checking if this agent should accept this call, backup it or redirect to mobile
function newIncomingCall(e){
  let direct_call = false;
  let identity;

  if(e.o_event.o_message.o_hdr_From.s_display_name && e.o_event.o_message.o_hdr_From.s_display_name.indexOf('direct_transfer') != -1) {

    callingVia = e.o_event.o_message.o_hdr_From.s_display_name.substring(e.o_event.o_message.o_hdr_From.s_display_name.indexOf(':\\"')+3);
    direct_call = true;

  } else if(e.o_event.o_message.o_hdr_From.s_display_name && e.o_event.o_message.o_hdr_From.s_display_name.indexOf('\\') != -1){
    callingVia = e.o_event.o_message.o_hdr_From.s_display_name.substring(e.o_event.o_message.o_hdr_From.s_display_name.indexOf('\\')+2, e.o_event.o_message.o_hdr_From.s_display_name.indexOf('\\', e.o_event.o_message.o_hdr_From.s_display_name.indexOf('\\') + 2));
  } else {
    callingVia = "unknown";
  }

  if(callingVia != 1 || callingVia == "unknown"){

    let from = e.newSession.o_session.o_uri_from.s_user_name.indexOf('anonymous') == -1 ? e.newSession.o_session.o_uri_from.s_user_name.replace('+', '') : 'anonymous';

    var refirectedFrom = false;

    if(from.indexOf(0) == 0) {
      from = from.substr(1);
      refirectedFrom = true;
    }

    let to = agent.publicid;

    let tempCallSession = e.newSession;

    tempCallSession.identity = callingVia == "unknown" ? {phonenumber: "unknown"} : agent.numbers_object[callingVia];

    let o_message = e.o_event.o_message;

    if(agent.numbers_object[callingVia]) {
      identity = agent.numbers_object[callingVia];
    } else {
      identity = false;
    }
    let data;
    if(from.indexOf('99') == 0){
      data = {ifInner: from.substr(0, 2), extension: chrome.extension.getURL('/')};
    } else{
      data = {numbers: [from, callingVia], ifExist: from, ifInner: from, callingVia, guid: agent.contactguid, prioritizing: [from, callingVia, agent.publicid], extension: chrome.extension.getURL('/')};
    }

    jQuery.ajax({
      type: "POST",
      url: 'https://www.interbooker.com/admin/voip3/checkCountry.php',
      data: data,
      dataType: 'json'
    }).done((res) => {
      console.log(res)

      let ifCall = {};
      //Access levels
      let fromInternalName = false;

      if(res['inner']) {

        ifCall.access = 'allow';
        callingVia = "Internal";

      } else {

        ifCall = checkingAccess(identity, res, from);

      }
      console.log(ifCall)
      if(((refirectedFrom && !res['arrival']) || (ifCall && (ifCall.access == 'allow' || ifCall.access == 'backup')) || direct_call) && !callingNow && from != '9010'){

        navigator.webkitGetUserMedia(
          {
              audio: true
          },
          function() {
            if(!(ifCall.access == 'backup' && waitingBackup)){

              if(ifCall.access == 'allow' || direct_call){
                if(res['inner'])
                  fromInternalName = o_message.o_hdr_From.s_display_name;
                initiateCall(from, callingVia, to, true, fromInternalName, tempCallSession);
              } else if(ifCall.access == 'backup') {
                clearTimeout(initiateCallTimeout);
                waitingBackup = true;
                backup = true;
                tempCallSession.setConfiguration({
                  audio_remote: document.getElementById('audio-remote'),
                  events_listener: { events: '*', listener: function(e){
                    console.info('INCALLING calling session event = ' + e.type);

                    if(e.type == 'terminated'){

                      backup = false;
                      tempCallSession = false;
                      status = 'connected';
                      clearTimeout(initiateCallTimeout);

                    }
                  }},
                });
                console.log('backUp initialized');
                let backupFrom = from;
                let backupCallingVia = callingVia;
                let backupTo = to;
                let backupInternal = fromInternalName;
                initiateCallTimeout = setTimeout(initiateCall, 20000, backupFrom, backupCallingVia, backupTo, false, backupInternal, tempCallSession);
              }

              //Saving call info
              let historyObj = {
                "contact": from,
                "company": callingVia,
                "agent": agent.publicid,
                "agent_id": agent.contact_id,
                "agent_name": agent.displayname,
                "answered":"0",
                "outbound":"0",
                "fromInternal": fromInternalName
              }

              setTimeout(saveCallInHistory, Math.random()*1000, historyObj);

            }
          },
          function(error) {
            console.log('Error:', error);
            ifCall = false;
            chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
          }
        );
      }
      //Call rejected
      else {

        var historyObj = {
          "contact": from,
          "company": callingVia,
          "agent": agent.publicid,
          "agent_id": agent.contact_id,
          "agent_name": agent.displayname,
          "answered":"0",
          "outbound":"0",
          "blocked":"2"
        }

        setTimeout(saveCallInHistory, Math.random()*1000, historyObj);

        if(from == '9010'){

          callConference({mobiles:['380930130214'],agentsBackup:[]}, '380947103090', from, tempCallSession);

        } else if(ifCall && ifCall.access == 'mobile'){

          callConference(ifCall, callingVia, from, tempCallSession);

        } else {
          tempCallSession.reject();
          tempCallSession = false;
        }
      }
    });
  }
}
//this function is triggered in case of succesfull incoming call and it's starting calltone, setting listeners for call etc...
function initiateCall(from, callingVia, to, allow, fromInternalName, tempCallSession){

  if(!callingNow && (allow || backup)){
    callingCounter = 0;
    callingCounting = true;
    incrementCallingTime();

    callSession[from] = tempCallSession;
    currentCounterparty = from;

    callSession[from].setConfiguration({
      audio_remote: document.getElementById('audio-remote'),
      events_listener: { events: '*', listener: function(e){
        console.info('INCALLING calling session event = ' + e.type);

        if(e.type == 'connected'){

          secondsCounter = 0;
          counting = true;
          isAnswered = true;

          //Duration counter
          incrementSeconds();
          callingCounter = 0;
          callingCounting = false;
          stopCallingtone();
          callingIcon = false;
          chrome.browserAction.setIcon({'path': "assets/icons/icon.png"});
          //saving history
          jQuery.post(
            'https://www.interbooker.com/admin/voip3/saveHistory.php',
            {historyID: callId[from], from, callingVia, answered: true, guid: agent.contactguid, agent: agent.publicid, extension: chrome.extension.getURL('/')}
          );

        } else if(e.type == 'terminated'){

          callingVia = null;
          callHangdUp();

        }

      }},
    });

    status = 'callingIn';
    callingNow = true;
    newOnline(from);
    prevWindow = currentWindow;
    currentWindow = 'callingBoxIn';
    callingIcon = true;
    changingIcon();
    timer = {
      hours: 0,
      minutes: 0,
      seconds: 0
    }
    chrome.runtime.sendMessage({key: 'runTimer', timer});
    chrome.runtime.sendMessage({key: 'onCallingIn', currentWindow: currentWindow, status, callSession: {counterparty: from, identity: callingVia, direction: 'in', fromInternalName}});
    startCallingtone();
  }
}

//checking access rights of current agent and agents that are online and decide if app should ring, backup or redirect to mobile
function checkingAccess(number, response, callerNumber){

  let access = {};
  let backup = false;
  let allow = false;

  let deprioritize = false;

  //Check if account has rights to recive incoming call
  if(agent.call_account == 1 && number){

    //if all rights
    if(number.accesslevel == '222222'){
      access.allow = true;
    }
    //if internal
    else if(response['inner']){
      access = checkAccessInstance(number.accesslevel[0]);
    }
    //if anonymous
    else if(callerNumber == 'anonymous'){
      access = checkAccessInstance(number.accesslevel[3]);
    }//if contacts and arrival
    else if(response['arrival']){
      access = checkAccessInstance(number.accesslevel[5]);
    }
    //if contacts and not arrival or there is not host number
    else if(response['exist']){
      access = checkAccessInstance(number.accesslevel[4]);
    }
    //if domestic
    else if(response.flag[0] == response.flag[1] && !response['exist']){
      access = checkAccessInstance(number.accesslevel[1]);
    }
    //if non-domestic
    else if(response.flag[0] != response.flag[1] && !response['exist']){
      access = checkAccessInstance(number.accesslevel[1]);
    }

    console.log('TTTTTTTTTTT')
    console.log(access)
    console.log(number)
    console.log(response)
    console.log(callerNumber)

    //checking priorities for allowed agent
    if(access && access.allow && number.priority && number.priority != '0' && (response.last_agent || response.deprioritize)){
      if(number.priority.indexOf(1) !== -1 && response.last_agent){
        if(response.last_agent == agent.publicid){
          return {access: 'allow'};
        } else {
          access.backup = true;
          deprioritize = true;
        }
      }
      if(number.priority.indexOf(3) !== -1 && response.deprioritize){
        if(response.deprioritize){
          return false;
        } else {
          return {access: 'allow'};
        }
      }
      if(number.priority.indexOf(2) !== -1 && response.deprioritize){
        if(response.deprioritize && response.online.length > 1){
          return {access: 'backup'};
        } else {
          return {access: 'allow'};
        }
      }
    } else if(access.allow){
      return {access: 'allow'};
    }
  }

  //Checkingg
  var voip = false;
  var deprioritizeVoip = false;
  var mobiles = [];
  var mobileBackup = [];
  var agentsBackup = [];

  Object.values(response.access).forEach(account => {
    if(account.levels == '222222'){
      if(account.voip){
        if(deprioritize && account.voip == response.last_agent){
          deprioritizeVoip = true;
        }
        voip = true;
      } else {
        account.mobiles.forEach(function(mobileToCall){
          if(mobiles.indexOf(mobileToCall) == -1)
            mobiles.push(mobileToCall);
        });
      }
    }
    //if internal
    else if(response['inner']){
      if(account.levels[0] == 2){
        if(account.voip){
          if(deprioritize && account.voip == response.last_agent){
            deprioritizeVoip = true;
          }
          voip = true;
        } else {
          account.mobiles.forEach(function(mobileToCall){
            if(mobiles.indexOf(mobileToCall) == -1)
              mobiles.push(mobileToCall);
          });
        }
      } else if(account.levels[0] == 1 && account.voip){

          agentsBackup.push(account.voip);

      }
    }
    //if anonymous
    else if(callerNumber == 'anonymous'){
      if(account.levels[3] == 2){
        if(account.voip){
          if(deprioritize && account.voip == response.last_agent){
            deprioritizeVoip = true;
          }
          voip = true;
        } else {
          account.mobiles.forEach(function(mobileToCall){
            if(mobiles.indexOf(mobileToCall) == -1)
              mobiles.push(mobileToCall);
          });
        }
      } else if(account.levels[3] == 1 && account.voip){
        agentsBackup.push(account.voip);
      }
    }
    //if contacts and not arrival or there is no host number
    else if(response['exist'] && !response['arrival']){

      if(account.levels[4] == 2){
        if(account.voip){
          if(deprioritize && account.voip == response.last_agent){
            deprioritizeVoip = true;
          }
          voip = true;
        } else {
          account.mobiles.forEach(function(mobileToCall){
            if(mobiles.indexOf(mobileToCall) == -1)
              mobiles.push(mobileToCall);
          });
        }
      } else if(account.levels[4] == 1 && account.voip){
        agentsBackup.push(account.voip);
      }

    }//if contacts and arrival
    else if(response['arrival']){

      if(account.levels[5] == 2){
        if(account.voip){
          if(deprioritize && account.voip == response.last_agent){
            deprioritizeVoip = true;
          }
          voip = true;
        } else {
          account.mobiles.forEach(function(mobileToCall){
            if(mobiles.indexOf(mobileToCall) == -1)
              mobiles.push(mobileToCall);
          });
        }
      } else if(account.levels[5] == 1 && account.voip){
        agentsBackup.push(account.voip);
      }

    }
    //if domestic
    else if(response.flag[0] == response.flag[1] && !response['exist'] && callerNumber != 'anonymous'){
      if(account.levels[1] == 2){
        if(account.voip){
          if(deprioritize && account.voip == response.last_agent){
            deprioritizeVoip = true;
          }
          voip = true;
        } else {
          account.mobiles.forEach(function(mobileToCall){
            if(mobiles.indexOf(mobileToCall) == -1)
              mobiles.push(mobileToCall);
          });
        }
      } else if(account.levels[1] == 1 && account.voip){
          agentsBackup.push(account.voip);
      }
    }
    //if non-domestic
    else if(response.flag[0] != response.flag[1] && !response['exist'] && callerNumber != 'anonymous'){
      if(account.levels[2] == 2){
        if(account.voip){
          if(deprioritize && account.voip == response.last_agent){
            deprioritizeVoip = true;
          }
          voip = true;
        } else {
          account.mobiles.forEach(function(mobileToCall){
            if(mobiles.indexOf(mobileToCall) == -1)
              mobiles.push(mobileToCall);
          });
        }
      } else if(account.levels[2] == 1 && account.voip){
        agentsBackup.push(account.voip);
      }
    }
  });
  console.log('PPPP');
  console.log(access);
  if(access && voip && deprioritize && deprioritizeVoip && number && agent.call_account == 1){
    return {access: 'backup'};
  } else if(access && voip && number && ((deprioritize && !deprioritizeVoip) || !deprioritize) && agent.call_account == 1){
    return {access: 'allow'};
  } else if(access && voip && number && agent.call_account == 1){
    return {access: 'backup'};
  } else if(mobiles.length > 0){
    let main_extension = callerNumber[callerNumber.length - 1];

    if(response.all_online.length - 1 < main_extension)
      main_extension = response.all_online.length - 1;
    console.log({access: 'mobile', mobiles, agentsBackup});
    if(response.all_online.indexOf(agent.contactguid) == main_extension){
      return {access: 'mobile', mobiles, agentsBackup};
    } else {
      return false;
    }
  } else if(access.backup && agent.call_account == 1) {
    return {access: 'allow'};
  }

  return false;

}

//Checking access rights of current agent
function checkAccessInstance(accesslevel){
    if(accesslevel == 2){
      return {allow: true};
    } else if(accesslevel == 1){
      return {backup: true};
    } else if(accesslevel == 0){
      return false;
    }
}

//Starting new outgoing call session
function calling(identity, counterparty){

  let counterpartyNumber = counterparty.number.replace(/ /g, '');
  let identityNumber = identity != 'inner' && identity.phonenumber ? identity.phonenumber.replace(/ /g, '') : identity != 'inner' ? identity : agent.publicid;

  currentCounterparty = counterpartyNumber;

		let callingEventsListener = function(e){
				console.info('OUTCALLING session event = ' + e.type);
        if(e.type == 'connected'){
          stopRingtone();
          secondsCounter = 0;
          counting = true;
          timerStart();
          isAnswered = true;
          chrome.runtime.sendMessage({key: 'startTimer'});
          jQuery.post(
            'https://www.interbooker.com/admin/voip3/saveHistory.php',
            {historyID: callId[counterpartyNumber], answered: true, outbound: true, guid: agent.contactguid, from: counterpartyNumber, callingVia: identityNumber, agent: agent.publicid, extension: chrome.extension.getURL('/')}
          );
          //Duration counter
          incrementSeconds();
        } else if(e.type == 'terminated') {

          if(secondsCounter == 0){
            jQuery.post(
              'https://www.interbooker.com/admin/voip3/saveHistory.php',
              {historyID: callId[counterpartyNumber], notAnswered: true, from: counterpartyNumber, callingVia: identityNumber, extension: chrome.extension.getURL('/')}
            );
          }
          callHangdUp();
				}
		}

		callSession[counterpartyNumber] = sipStack.newSession('call-audio', {
				audio_remote: document.getElementById('audio-remote'),
        sip_headers: [
            {name: 'called_by', value: agent.publicid, session: false},
            {name: 'called_to', value: counterpartyNumber, session: false}
        ],
				events_listener: { events: '*', listener: callingEventsListener }
		});

    callSession[counterpartyNumber].counterparty = counterparty;
    callSession[counterpartyNumber].identity = identity != 'inner' ? identity : 'inner';

    if(identityNumber != agent.publicid){
  		numberReplacement(identityNumber, '99');
    } else {
    	numberReplacement(identityNumber);
      identityNumber = "Internal";
    }

    status = 'callingOut';
    newOnline();

		callSession[counterpartyNumber].call(counterpartyNumber);

    callingNow = true;
    startRingtone();

    //Saving call info
    let historyObj = {
      "contact": counterpartyNumber,
      "company": identityNumber,
      "agent": agent.publicid,
      "agent_id": agent.contact_id,
      "agent_name": agent.displayname,
      "answered":"0",
      "outbound":"1",
    }
    saveCallInHistory(historyObj);

}

//trigered when incomin or outgoing call is finished
function callHangdUp(){
  currentWindow = prevWindow;
  callingIcon = false;
  callingCounter = 0;
  callingCounting = false;
  callingNow = false;
  backup = false;
  status = 'connected';
  newOnline();
  chrome.browserAction.setIcon({'path': "assets/icons/icon.png"});
  chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow, status});
  chrome.runtime.sendMessage({key: 'onClearCall'});
  stopCallingtone();
  stopRingtone();
  stopTimer();
  if(secondsCounter > 0 && isAnswered){
    jQuery.post(
      'https://www.interbooker.com/admin/voip3/saveHistory.php',
      {historyID: callId[currentCounterparty], timeSec: secondsCounter, extension: chrome.extension.getURL('/')}
    );
    sipStack.stop();
  }
  counting = false;
  secondsCounter = 0;
  isAnswered = false;
  waitingBackup = false;
  delete callSession[currentCounterparty];
  currentCounterparty = false;
  clearTimeout(initiateCallTimeout);

}

function online(){
  clearTimeout(onlineTimeout);
  jQuery.post(
    'https://www.interbooker.com/admin/voip3/updateOnline.php',
    {guid: agent.contactguid, number: agent.publicid, status, extension: chrome.extension.getURL('/')},
    onlineAgents => {
      let online_agents = JSON.parse(onlineAgents);
      chrome.runtime.sendMessage({key: 'onlineAgents', online_agents})
      if(!online_agents[agent.contactguid] && status == 'connected'){
        chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
      }
    }
  );
  if(status == 'connected' && (!SIPml.isInitialized() || !SIPml.isReady())){
    status = 'connecting';
    sipStack.stop();
  }
  onlineTimeout = setTimeout(online, 3000);
}
//saving to DB current status of extension and taking other online agents
function newOnline(called_from = 0){
  jQuery.post(
    'https://www.interbooker.com/admin/voip3/updateOnline.php',
    {guid: agent.contactguid, called_from, number: agent.publicid, status, extension: chrome.extension.getURL('/')},
    onlineAgents => {
      let online_agents = JSON.parse(onlineAgents);
      chrome.runtime.sendMessage({key: 'onlineAgents', online_agents})
      if(!online_agents[agent.contactguid] && status == 'connected'){
        chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
      }
    }
  );
}
//saving to DB current status of extension and taking other online agents
function saveCallInHistory(historyObj){
  jQuery.post(
    'https://www.interbooker.com/admin/voip3/saveHistory.php',
    {history: historyObj, extension: chrome.extension.getURL('/')}
  ).done(function(res) {

      callId[historyObj.contact] = res;

  });
}

//Blicking icon while incoming call
function changingIcon(){
  if(callingIcon && iconColor == 'dark'){
    chrome.browserAction.setIcon({'path': "assets/icons/icon2.png"});
    iconColor = 'white';
    setTimeout(changingIcon, 300);
  } else if(callingIcon) {
    chrome.browserAction.setIcon({'path': "assets/icons/icon1.png"});
    iconColor = 'dark';
    setTimeout(changingIcon, 300);
  }
}

//starting timer for duration of call
function timerStart(){

  clearInterval(timerInterval);

  timer = {
    hours: 0,
    minutes: 0,
    seconds: 0
  };

  timerInterval = setInterval(function(){
    if(timer.seconds < 59){
      timer.seconds++;
    } else {
      if(timer.minutes < 59){
          timer.minutes++;
          timer.seconds = 0;
      } else {
          timer.hours++;
          timer.minutes = 0;
          timer.seconds = 0;
      }
    }
  }, 1000);
}

function stopTimer(){
  clearInterval(timerInterval);

  timer = {
    hours: 0,
    minutes: 0,
    seconds: 0
  }
}

//Counting secconds for call
function incrementSeconds() {
    secondsCounter++;
    if(counting){
      setTimeout(incrementSeconds, 1000);
    }
}

//Counting calling time
function incrementCallingTime() {
    callingCounter++;
    if(callingCounting && callingCounter < 60){
      setTimeout(incrementCallingTime, 1000);
    } else if(callingCounter == 60) {
      callingCounting = 0;
      callingCounting = false;
      callSession[currentCounterparty].reject();
    }
}

//replacement of extension numbers(needed to choose from what number you wish to call)
function numberReplacement(number, prepend = ''){
  SIPml.Stack.prototype.ao_sessions.forEach(session => {
      if(session && session.o_session.o_stack.identity.s_display_name == agent.publicid){
          session.o_session.o_uri_from.s_user_name = prepend + number;
          session.o_session.o_stack.identity.s_user_name = number;
          session.o_session.o_stack.identity.s_impi = prepend + number;
      }else{
          console.log("DEFOULT DATA FOR SIPML5");
      }
  });
}

//Starting/Stoping of ringtones
function startRingtone(){
  ringtone.play();
}
function startCallingtone(){
  callingtone.play();
}
function stopRingtone(){
  ringtone.pause();
}
function stopCallingtone(){
  callingtone.pause();
}
