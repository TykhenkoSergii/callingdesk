let audio = document.getElementById("audio_remote");
let ringtone = document.getElementById("ringtone");
let callingtone = document.getElementById("callingtone");
let stackConnected = false;
let agent, status, sipStack, registerSession, transferStack, prevWindow, callHistory, callingCounter, callingCounting, beforeHold, callingIcon, callingNow, backup, transferEventsListener, transferStackListener;
let transferRegisterSession, transfered, closedTransferCalls, transferCallSession, isHostCallAccepted, dtmf, calledBackup, waitingBackup, restartedAt;
let initiateCallTimeout, timerInterval, onlineTimeout, soundTimeout, conferenceParticipants, conferenceNumber, conferenceLastNum, currentCounterparty;
let callSession = {};
let callChannels = {};
let callId = {};
let timer = {
  hours: 0,
  minutes: 0,
  seconds: 0
};
let secondsCounter = 0;
let currentWindow = 'calling';
let counting = false;
let isAnswered = false;
let iconColor = 'dark';
let callingVia;
let soundStatus = true;

if(!agent && !sipStack){
  chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
}

navigator.webkitGetUserMedia(
    {
        audio: true
    },
    function(stream) {

      console.log('Stream:', stream);
    },
    function(error) {
    	console.log('Error:', error);
      chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
    }
);
//start ping for restarting sipStack
setTimeout(ping, 3600000);

//CREATE Stack
const createStack = (agent) => {

  //SIPml.setDebugLevel('fatal');
	sipStack = createNewSipStack(agent.server, agent.publicid, agent.password, stackListeners.sipStack);

	sipStack.start();
	SIPml.init(sipStack);

}

//Listen for changes from front-end
chrome.extension.onMessage.addListener(function(request, sender, callback){
	switch (request.key) {
    //checking if agent exists, this is the same agent, that was loged in before and return agent, or callSession even if agent is another
		case 'checkAgent':
      //checking if agent exists and it's the same agent
			if(agent && agent.contactguid == request.guid){

        if(callSession[currentCounterparty] && status == 'callingOut'){
          callback({isNew: false, status, agent, callSession: {counterparty: callSession[currentCounterparty].o_session.o_uri_to.s_user_name, identity: callSession[currentCounterparty].identity.phonenumber, direction: 'out'}});
          chrome.runtime.sendMessage({key: 'runTimer', timer});
        } else if(callSession[currentCounterparty] && status == 'callingIn') {
          callback({isNew: false, status, agent, callSession: {counterparty: callSession[currentCounterparty].o_session.o_uri_from.s_user_name, identity: callSession[currentCounterparty].identity.phonenumber, direction: 'in'}});
          chrome.runtime.sendMessage({key: 'runTimer', timer});
        } else if(callSession[currentCounterparty] && status == 'conference') {
			    callback({isNew: false, status, agent, conferenceParticipants, callSession: {counterparty: callSession[currentCounterparty].counterparty, identity: callSession[currentCounterparty].identity, direction: 'out'}});
        } else {
			    callback({isNew: false, status, agent, callSession: false});
        }
      //return callSession even if agent exists but is another then loged in
			} else if(agent && (status == 'callingOut' || status == 'callingIn' || status == 'conference')) {

        callback({agent});
        chrome.runtime.sendMessage({key: 'runTimer', timer});
      //deleting current agent(if exists) and returning status that new agent info is needed
      } else {
        agent = false;
				status = false;
				callback({isNew: true});
			}
      chrome.browserAction.setIcon({'path': "assets/icons/icon.png"});
      chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: currentWindow, status});
			break;
    //saving new agent info to background
		case 'newAgent':
			agent = request.agent;
      newOnline();
			createStack(agent);
			break;

    case 'checkWindow':

      if(callSession[currentCounterparty] && status == 'callingOut'){
        callback({currentWindow, status, soundStatus, callSession: {counterparty: callSession[currentCounterparty].o_session.o_uri_to.s_user_name, identity: callSession[currentCounterparty].identity.phonenumber, direction: 'out'}});
      } else if(callSession[currentCounterparty] && status == 'callingIn') {
        callback({currentWindow, status, soundStatus, callSession: {counterparty: callSession[currentCounterparty].o_session.o_uri_from.s_user_name, identity: callSession[currentCounterparty].identity.phonenumber, direction: 'in'}});
      } else if(status == 'conference') {
        callback({currentWindow, status, soundStatus, conferenceParticipants, callSession: {counterparty: callSession[currentCounterparty].counterparty, identity: callSession[currentCounterparty].identity, direction: 'out'}});
      } else {
        callback({currentWindow, status, soundStatus});
      }
      break;
    //delete agent info from background
    case 'deleteAgent':
      if(!callSession[currentCounterparty]){
			   agent = false;
      }
			break;
    //check if history is saved and return it(not implemented for now)
    case 'checkHistory':
      if(callHistory){
        callback({callHistory: callHistory});
      } else {
        callback({callHistory: false});
      }
			break;
    //saving history to background(not implemented for now)
    case 'loadHistory':
      callHistory = request.callHistory;
			break;
    //starting outgoing call
		case 'initCall':
			calling(request.identity, request.counterparty);
      prevWindow = currentWindow;
      currentWindow = 'callingBoxOut';
      status = 'callingOut';
      newOnline();
			chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: currentWindow, status});

			break;
    //transfering call to another number
    case 'transferCall':
      if(callSession[currentCounterparty]){
        callSession[currentCounterparty].transfer(request.number);
        jQuery.post(
          'https://www.interbooker.com/admin/voip3/saveHistory.php',
          {historyID: callId[callSession[currentCounterparty].o_session.o_uri_to.s_user_name], transfered: request.number, agent: agent.publicid, extension: chrome.extension.getURL('/')}
        );
        currentWindow = prevWindow;
        status = 'connected';
        newOnline();
  			chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: currentWindow, status});
        callSession[currentCounterparty].hangup();
    		delete callSession[currentCounterparty];
        callingNow = false;
      }

			break;
    //transfering call to conference and automaticly calling to that conference
    case 'toConference':
      if(callSession[currentCounterparty]){
        jQuery.ajax({
          type: "POST",
          url: 'https://www.interbooker.com/admin/voip3/getTransferNumber.php',
          data: {extension: chrome.extension.getURL('/')},
          dataType: 'json'
        })
        .done(resultNumber => {
          resultNumber = resultNumber.toString();
          if(resultNumber[resultNumber.length-1] == '9'){
            conferenceLastNum = 1;
            conferenceNumber = resultNumber.replace('9', '1');
          } else {
            conferenceLastNum = +resultNumber[resultNumber.length-1] + 1;
            conferenceNumber = resultNumber.substr(0, resultNumber.length-1) + conferenceLastNum;
          }
          let isTransfered = false;
          callSession[currentCounterparty].setConfiguration({
            events_listener: { events: '*', listener: function(e){
        				console.info('TO CONFERENCE session event = ' + e.type);
                if((e.type == 'terminated' || e.type == 'o_ect_notify') && !isTransfered){
                  isTransfered = true;
                  callToConference(conferenceNumber);
        				}
        		}}
          });
          conferenceParticipants = {};
          conferenceParticipants[request.participant.number] = {...request.participant, status: 'connected', sip_channel: callChannels[request.participant.number] ? callChannels[request.participant.number] : 0};

          callSession[currentCounterparty].transfer(conferenceNumber);
          status = 'conference';
          currentWindow = 'callingBoxOut';
          newOnline();
          chrome.runtime.sendMessage({key: 'onCallingConference', currentWindow, status, conferenceParticipants, callSession: {counterparty: conferenceNumber, identity: agent.publicid, direction: 'out'}});
        });
      }
			break;
    //adding another participant to conference
    case 'addToConference':

      addToConference(request.number, request.counterparty);

      conferenceParticipants[request.counterparty.number] = {...request.counterparty, status: 'calling'};

      status = 'conference';
      currentWindow = 'callingBoxOut';

      chrome.runtime.sendMessage({key: 'onAddToConference', currentWindow, status, conferenceParticipants});

      break;
    //saving changes about conference participant
    case 'changeConferenceParticipant':
      conferenceParticipants[request.participant.number] = {...conferenceParticipants[request.participant.number], ...request.participant};

      chrome.runtime.sendMessage({ key: 'onConferenceParticipantChange', conferenceParticipants });

      break;
    //throwing out participant from conference
    case 'throwParticipant':

      let throwParticipant = sipStack.newSession('call-audio', {
          sip_headers: [
              {name: 'action', value: 'hangup_participant', session: false},
              {name: 'hangup_call', value: conferenceParticipants[request.number].sip_channel, session: false}
          ]
      });

      throwParticipant.call(agent.publicid);

      delete conferenceParticipants[request.number];

      status = 'conference';
      currentWindow = 'callingBoxOut';

      chrome.runtime.sendMessage({key: 'onAddToConference', currentWindow, status, conferenceParticipants});

      break;
    //hanging up current call
		case 'hangUp':
      currentWindow = prevWindow;
      status = 'connected';
      newOnline();
			chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: currentWindow, status});
      var hangUpError = false;
      if((status == 'callingBoxIn' || status == 'onHoldIn') && isAnswered){
        try{
          var hangUpResult = callSession[currentCounterparty].hangup();
        } catch(err) {
          hangUpError = err;
        }
      } else if(status == 'callingBoxIn' || status == 'onHoldIn') {

        try{
          var hangUpResult = callSession[currentCounterparty].reject();
        } catch(err) {
          hangUpError = err;
        }
  		  delete callSession[currentCounterparty];
      } else {
        try{
          var hangUpResult = callSession[currentCounterparty].hangup();
        } catch(err) {
          hangUpError = err;
        }
      }
      console.log(hangUpError);
      callingNow = false;
      if(hangUpResult == -1 || hangUpError.length > 0){
        callingIcon = false;
        chrome.browserAction.setIcon({'path': "assets/icons/icon.png"});
        callingVia = null;
        delete callSession[currentCounterparty];
        stopCallingtone();
        if(secondsCounter > 0 && isAnswered){
          jQuery.post(
            'https://www.interbooker.com/admin/voip3/saveHistory.php',
            {historyID: callId[callSession[currentCounterparty].o_session.o_uri_to.s_user_name], timeSec: secondsCounter, extension: chrome.extension.getURL('/')}
          );
        }
        counting = false;
        secondsCounter = 0;
        isAnswered = false;
        currentWindow = prevWindow;
        status = 'connected';

        chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow, status});
      }
			break;

    case 'hangUpConference':
      callSession[currentCounterparty].hangup({
          sip_headers: [
              {name: 'action', value: 'hangup_conference', session: false},
              {name: 'hangup_conference', value: currentCounterparty, session: false}
          ]

      });
      chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow, status});
      break;

    case 'acceptCall':
      callSession[currentCounterparty].accept();
      timerStart();

			break;

    case 'holdCall':
      callSession[currentCounterparty].hold();
      beforeHold = currentWindow;
      currentWindow = currentWindow == 'callingBoxIn' ? 'onHoldIn' : currentWindow == 'conference' ? 'onConferenceHold' : 'onHoldOut';
      status = currentWindow == 'callingBoxIn' ? 'onHoldIn' : currentWindow == 'conference' ? 'onConferenceHold' : 'onHoldOut';
      chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: currentWindow, status});
			break;

    case 'resumeCall':
      callSession[currentCounterparty].resume();
      currentWindow = beforeHold;
      status = currentWindow == 'callingBoxIn' ? 'callingIn' : 'callingOut';
      chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: currentWindow, status});
			break;

		case 'changeWindow':
      currentWindow = request.currentWindow;
			chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: request.currentWindow, status});

			break;
    //muting calingtone sound(just for 30 seconds)
    case 'changeSoundStatus':

      soundStatus = request.soundStatus;
      clearTimeout(soundTimeout);
      if(soundStatus){
        callingtone.volume = 1;
      } else {
        callingtone.volume = 0.1;
        soundTimeout = setTimeout(function(){
          callingtone.volume = 1;
          soundStatus = true;
        }, 300000)

      }
      break;

		case 'dtmfclick':
			callSession[currentCounterparty].dtmf(request.dialKey);

			break;

		default:
			callback({currentWindow: currentWindow, status});

	}
});

//Listeners for message from interbooker site.
//It's used for call button
chrome.runtime.onMessageExternal.addListener(function(request) {

  switch(request.key){
    case 'siteCallBtn':
      if(request.outboundType == 'mobile'){
        if(!sipStack){
          agent = request.agent;
          newOnline();
          createStack(request.agent);
          let mobileInterval = setInterval(function(){
            if(status == 'connected'){
              mobileToMobile(request.identity, request.counterparty, request.agentMobile);
              clearInterval(mobileInterval);
            }
          }, 500);

        } else {
          if(currentCounterparty)
            callSession[currentCounterparty].hangup();
          mobileToMobile(request.identity, request.counterparty, request.agentMobile);

        }
      } else {
        window.open("index.html", "extension_popup", "width=780,height=640,status=no,scrollbars=no,resizable=no");

        var callBtnInterval = setInterval(sendContactId, 500, request);

        function sendContactId({counterparty, identity, agent}){

          if(currentCounterparty)
            callSession[currentCounterparty].hangup();

          if(status == 'connected'){
            if(identity){

              chrome.extension.sendMessage({key: 'onSiteCallBtn', counterparty, identity, agent}, function(checkRes){

                if(checkRes == 'success')
                  clearInterval(callBtnInterval);
              });
            } else {
              chrome.extension.sendMessage({key: 'onSiteCallBtn', counterparty}, function(checkRes){

                if(checkRes == 'success')
                  clearInterval(callBtnInterval);
              });
            }
          }
        }
      }
      break;

    default:

  }

});
