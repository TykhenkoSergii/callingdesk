
//Creating call conference for redirected call
function callConference(callInfo, callingVia, callFrom, incomingCallSession){
  let availableAgents = callInfo.agentsBackup;
  let mobiles = callInfo.mobiles;
  let lastNum;

  jQuery.ajax({
    type: "POST",
    url: 'https://www.interbooker.com/admin/voip3/getTransferNumber.php',
    data: {extension: chrome.extension.getURL('/')},
    dataType: 'json'
  })
  .done(resultNumber => {
    resultNumber = resultNumber.toString();
    if(resultNumber[resultNumber.length-1] == '9'){
      lastNum = 1;
      conferenceNumber = resultNumber.replace('9', '1');
    } else {
      lastNum = +resultNumber[resultNumber.length-1] + 1;
      conferenceNumber = resultNumber.substr(0, resultNumber.length-1) + lastNum;
    }

    transferStack = [];
    transferCallSession = [];
    transferEventsListener = [];
    transferStackListener = [];
    transferRegisterSession = [];
    isHostCallAccepted = [];
    closedTransferCalls = [];
    transfered = false;
    calledBackup = false;

    incomingCallSession.setConfiguration({
      events_listener: { events: '*', listener: function(e){
        console.info('INCALLING TRANSFER calling session event = ' + e.type);
        if(e.type == 'connected'){

          jQuery.post(
            'https://www.interbooker.com/admin/voip3/saveHistory.php',
            {historyID: callId[callFrom], participant: agent.publicid, conference: conferenceNumber, extension: chrome.extension.getURL('/')}
          );

          setTimeout(function(){
            incomingCallSession.transfer(conferenceNumber);
          }, 400);

        } else if(e.type == 'terminated'){
          incomingCallSession = false;

          newTransferCall(lastNum, mobiles, callFrom, callingVia, conferenceNumber, availableAgents);

          setTimeout(function(){
            if(!transfered){

              transferCallSession.forEach(function(transferCallSessionEl){
                if(transferCallSessionEl)
                  transferCallSessionEl.hangup();
              });

            }
          }, 30000);

        }
      }},
    });

    incomingCallSession.accept();

  });
}

function newTransferCall(lastNum, mobiles, callFrom, callingVia, conferenceNumber, availableAgents){
  let currentSip = transferStack.length;
  let extension = '999' + lastNum + currentSip;
  dtmf = '1';

  jQuery.post(
    'https://www.interbooker.com/admin/voip3/saveHistory.php',
    {historyID: callId[callFrom], transfered: mobiles[currentSip], agent: agent.publicid, extension: chrome.extension.getURL('/')}
  );


  transferStack[currentSip] = new SIPml.Stack({
    realm: agent.server,
    impi: extension,
    impu: 'sip:'+extension+'@callingdesk.com',
    password: agent.password,
    display_name: extension,
    enable_rtcweb_breaker: true,
    bandwidth: { audio:64 },
    ice_servers: [{url:'stun:stun.patlive.com:3478'}],
    outbound_proxy_url:'udp://callingdesk.com:5060',
    websocket_proxy_url: 'wss://callingdesk.com:8089/ws',
    events_listener: { events: '*',
      listener: function(e){
        console.info('OUTCALLING'+currentSip+' stack session event = ' + e.type);

        if(e.type == 'failed_to_start'){

        } else if(e.type == 'started'){
          login();


        } else if(e.type == 'i_new_message' && e.getContentString().indexOf('accepted_by_host')){
            isHostCallAccepted[currentSip] = true;
				}
      }
    }
  });
  var eventsListener = function(e){
			console.info('LOGIN TRANSFER session event = ' + e.type);
			if(e.type == 'connected'){

        transferEventsListener[currentSip] = function(transferE){
            console.info('OUTCALLING'+currentSip+' TRANSFER session event = ' + transferE.type);
            if(transferE.type == 'connected'){
              if(isHostCallAccepted[currentSip]){

                transferCallSession[currentSip].transfer(conferenceNumber);

                transfered = true;

                jQuery.post(
                  'https://www.interbooker.com/admin/voip3/saveHistory.php',
                  {historyID: callId[callFrom], transfered: mobiles[currentSip], transfer_answered: true, agent: agent.publicid, extension: chrome.extension.getURL('/')}
                );

                transferCallSession.forEach(function(transferCallSessionEl){
                  if(transferCallSession[currentSip] && transferCallSessionEl != transferCallSession[currentSip])
                    transferCallSessionEl.hangup();
                });

              } else {
                transferCallSession[currentSip].hangup();
              }
            } else if(transferE.type == 'i_ao_request'){

              if(transferStack.length < mobiles.length)

                newTransferCall(lastNum, mobiles, callFrom, callingVia, conferenceNumber, availableAgents);

            } else if(transferE.type == 'terminated'){

              if((mobiles.length == 1 || (closedTransferCalls.length == mobiles.length - 1 && closedTransferCalls.indexOf(extension) == -1)) && !calledBackup && availableAgents && !transfered){
                dtmf = 0;

                callBackupAgents(transferStack[currentSip], callingVia, availableAgents, conferenceNumber, callFrom);
                calledBackup = true;

              } else {
                closedTransferCalls.push(extension);

                transferCallSession[currentSip] = false;
                transferStack[currentSip].stop();
                transferStack[currentSip] = false;
              }

            }
        }
        const transferHeaders = [
          {name: 'numb', value: callFrom, session: false},
          {name: 'dtmf', value: dtmf, session: false},
          {name: 'transfer_extension', value: extension, session: false}
        ];

        if(availableAgents.length == 0) transferHeaders.push({name: 'answer_call', value: conferenceNumber.toString(), session: false});

        transferCallSession[currentSip] = transferStack[currentSip].newSession('call-audio', {
            events_listener: { events: '*', listener: transferEventsListener[currentSip] },
            sip_headers: transferHeaders
        });

        for(var key in SIPml.Stack.prototype.ao_sessions){
            if(SIPml.Stack.prototype.ao_sessions[key]){
              if(true && SIPml.Stack.prototype.ao_sessions[key].o_session.o_stack.identity.s_display_name == extension){
                SIPml.Stack.prototype.ao_sessions[key].o_session.o_uri_from.s_user_name='99'+callingVia;
                SIPml.Stack.prototype.ao_sessions[key].o_session.o_uri_from.s_display_name=callingVia;
                SIPml.Stack.prototype.ao_sessions[key].o_session.o_stack.identity.s_user_name='99'+callingVia;
                SIPml.Stack.prototype.ao_sessions[key].o_session.o_stack.identity.s_impi='99'+callingVia;
                SIPml.Stack.prototype.ao_sessions[key].o_session.o_stack.identity.s_display_name=callingVia;

              } else{
                  console.log("DEFOULT DATA FOR SIPML5");
              }
            }
        }
        transferCallSession[currentSip].call('' + mobiles[currentSip]);

			} else if(e.type == 'terminated'){

      }
	}
  var login = function(){
			registerSession = transferStack[currentSip].newSession('register', {
					events_listener: { events: '*', listener: eventsListener } // optional: '*' means all events
			});
			registerSession.register();
	}

  transferStack[currentSip].start();
  SIPml.init(transferStack[currentSip]);

}

function callToConference(conferenceNumber){
		let callingEventsListener = function(e){
				console.info('OUTCALLING session event = ' + e.type);
        if(e.type == 'connected'){
          stopRingtone();
          timerStart();
          chrome.runtime.sendMessage({key: 'startTimer'});
          // jQuery.post(
          //   'https://www.interbooker.com/admin/voip3/saveHistory.php',
          //   {historyID: callId[conferenceNumber], answered: true, guid: agent.contactguid, from: conferenceNumber, callingVia: conferenceNumber, agent: agent.publicid, extension: chrome.extension.getURL('/')}
          // );
          //Duration counter
          incrementSeconds();
        } else if(e.type == 'terminating'){
          stopRingtone();
          currentWindow = prevWindow;
          status = 'connected';
          newOnline();
  			  chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow: currentWindow, status});
          counting = false;
          if(secondsCounter > 0){
            // jQuery.post(
            //   'https://www.interbooker.com/admin/voip3/saveHistory.php',
            //   {historyID: callId[conferenceNumber], timeSec: secondsCounter, extension: chrome.extension.getURL('/')}
            // );
          } else {
            // jQuery.post(
            //   'https://www.interbooker.com/admin/voip3/saveHistory.php',
            //   {historyID: callId[conferenceNumber], notAnswered: true, from: conferenceNumber, callingVia: conferenceNumber, extension: chrome.extension.getURL('/')}
            // );
          }
				} else if(e.type == 'terminated'){
					delete callSession[conferenceNumber];
          callingNow = false;
          status = 'connected';
          newOnline();
          stopRingtone();
          stopTimer();
          currentWindow = prevWindow;
  			  chrome.runtime.sendMessage({key: 'onChangeWindow', currentWindow, status});
          counting = false;
				}
		}

    currentCounterparty = conferenceNumber;

		callSession[conferenceNumber] = sipStack.newSession('call-audio', {
				audio_remote: document.getElementById('audio-remote'),
				events_listener: { events: '*', listener: callingEventsListener }
		});

    callSession[conferenceNumber].counterparty = conferenceNumber;
    callSession[conferenceNumber].identity = agent.publicid;

    conferenceNumber = conferenceNumber.replace(/ /g, '');
    callingNow = true;
    startRingtone();

		callSession[conferenceNumber].call(conferenceNumber);


}

function addToConference(number, counterparty){

  let isConnected = false;

  let callingEventsListener = function(e){
      console.info('OUTCALLING session event = ' + e.type);
      if(e.type == 'connected'){

        newCallSession.transfer(conferenceNumber);

        conferenceParticipants[counterparty.number] = {...conferenceParticipants[counterparty.number], status: 'connected'};

        chrome.runtime.sendMessage({ key: 'onConferenceParticipantChange', conferenceParticipants });
        isConnected = true;

      } else if(e.type == 'terminated' && !isConnected){

        delete conferenceParticipants[counterparty.number];
        chrome.runtime.sendMessage({ key: 'onConferenceParticipantChange', conferenceParticipants });

      }
  }

  let newCallSession = sipStack.newSession('call-audio', {
      audio_remote: document.getElementById('audio-remote'),
      sip_headers: [
          {name: 'called_by', value: agent.publicid, session: false},
          {name: 'called_to', value: counterparty.number, session: false}
      ],
      events_listener: { events: '*', listener: callingEventsListener }
  });

  counterparty.number = counterparty.number.replace(/ /g, '');

  numberReplacement(number, '99');

  newCallSession.call(counterparty.number);

}

function mobileToMobile(identity, counterparty, agentMobile){

  let conferenceNumber;
  let identityNumber = identity.publicid;
  agentMobile = agentMobile.replace(/ /g, '');
  let counterpartyNumber = counterparty.publicid.replace(/ /g, '');
  let lastNum = 1;

	let callingEventsListener = function(e){
			console.info('TRANSFER session event = ' + e.type);

      if(e.type == 'connected'){

        jQuery.ajax({
          type: "POST",
          url: 'https://www.interbooker.com/admin/voip3/getTransferNumber.php',
          data: {extension: chrome.extension.getURL('/')},
          dataType: 'json'
        })
        .done(resultNumber => {
          resultNumber = resultNumber.toString();
          if(resultNumber[resultNumber.length-1] == '9'){
            conferenceNumber = resultNumber.replace('9', '1');
          } else {
            lastNum = +resultNumber[resultNumber.length-1] + 1;
            conferenceNumber = resultNumber.substr(0, resultNumber.length-1) + lastNum;
          }

          //Saving call info
          var historyObj = {
            "contact": counterpartyNumber,
            "company": identityNumber,
            "agent": agent.publicid,
            "agent_id": agent.contact_id,
            "agent_name": agent.displayname,
            "conference": conferenceNumber,
            "answered":"0",
            "outbound":"2",
          }
          saveCallInHistory(historyObj);

          transferCallSession.transfer(conferenceNumber);

        });

      } else if(e.type == 'o_ect_accepted'){
        transferCallSession.hangup();
        var callingEventsListener = function(e){
    				console.info('TRANSFER session event = ' + e.type);
            if(e.type == 'connected'){
              // jQuery.post(
              //   'https://www.interbooker.com/admin/voip3/saveHistory.php',
              //   {historyID: callId[counterpartyNumber], answered: true, extension: chrome.extension.getURL('/')}
              // );

              transferCallSession.transfer(conferenceNumber);

            } else if(e.type == 'terminating'){


              transferCallSession = [];
    				} else if(e.type == 'terminated'){
              transferCallSession = [];
    				}
    		}

        transferCallSession = sipStack.newSession('call-audio', {
            sip_headers: [
                {name: 'dtmf', value: '1', session: false},
                {name: 'transfer_extension', value: agent.publicid, session: false},
                {name: 'answer_call', value: conferenceNumber.toString(), session: false}
            ],
    				events_listener: { events: '*', listener: callingEventsListener }
    		});

        numberReplacement(identityNumber, '99');

    		transferCallSession.call(counterpartyNumber);
			}
	}

	let transferCallSession = sipStack.newSession('call-audio', {
      sip_headers: [
          {name: 'dtmf', value: '1', session: false},
          {name: 'transfer_extension', value: agent.publicid, session: false}
      ],
			events_listener: { events: '*', listener: callingEventsListener }
	});

	numberReplacement(identityNumber, '99');

	transferCallSession.call(agentMobile);

}

function callBackupAgents(lastTransferSession, callingVia, availableAgents, conferenceNumber, callFrom){

  var agentsHangUp = [];

  availableAgents.forEach((availableAgent, i) => {

    var callingEventsListener = function(e){
        console.info('OUTCALLING session event = ' + e.type);
        if(e.type == 'connected'){

          transferCallSession[i].transfer(conferenceNumber);
          transferCallSession.forEach(function(conferenceNumberOne, j){
            if(j != i)
              conferenceNumberOne.hangup();
          });

        } else if(e.type == 'terminated'){
          agentsHangUp.push('1');
          if(agentsHangUp.length == availableAgents.length){

              lastTransferSession.stop();

          }
        }
    }
    transferCallSession[i] = lastTransferSession.newSession('call-audio', {
        events_listener: { events: '*', listener: callingEventsListener },
        sip_headers: [
            {name: 'transfer_backup_extension', value: callingVia, session: false},
            {name: 'transfer_numb', value: callFrom, session: false}
        ]
    });

    transferCallSession[i].call(availableAgent);

  });

}
