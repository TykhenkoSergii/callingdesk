const stackListeners = {
  
  sipStack: { events: '*',
    listener: function(e){
      console.info('stack session event = ' + e.type);
      console.log(e);
      if(e.type == 'failed_to_start'){
        chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
        setTimeout(createStack, 1000, agent);
      } else if(e.type == 'started'){

        setTimeout(login, 500, sipStack);

      } else if((e.type == 'terminated' || e.type == 'stopped') && (!restartedAt || restartedAt < Date.now() - 1500)){
        restartedAt = Date.now();
        chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
        setTimeout(createStack, 1000, agent);

      } else if(e.type == 'i_new_message'){
        let receivedMessage = e.getContentString();
        if(receivedMessage.indexOf('accepted_by_host') != -1){

        } else if(receivedMessage.indexOf('hangup') != -1){

          receivedMessage = JSON.parse(receivedMessage);

          if(conferenceParticipants && conferenceParticipants[receivedMessage.number]){
            delete conferenceParticipants[receivedMessage.number];

            chrome.runtime.sendMessage({ key: 'onConferenceParticipantChange', conferenceParticipants });
          }

        } else if(receivedMessage.indexOf('sip_channel') != -1){

          receivedMessage = JSON.parse(receivedMessage);

          if(conferenceParticipants && conferenceParticipants[receivedMessage.number]){
            conferenceParticipants[receivedMessage.number].sip_channel = receivedMessage.sip_channel;
          } else {
            callChannels[receivedMessage.number] = receivedMessage.sip_channel;
          }

        }

      } else if(e.type == 'm_permission_refused'){
        var hangUpError = false;
        if((status == 'callingBoxIn' || status == 'onHoldIn') && isAnswered && callSession[currentCounterparty]){
          try{
            var hangUpResult = callSession[currentCounterparty].hangup();
          } catch(err) {
            hangUpError = err;
          }
        } else if((status == 'callingBoxIn' || status == 'onHoldIn') && callSession[currentCounterparty]) {
          try{
            var hangUpResult = callSession[currentCounterparty].reject();
          } catch(err) {
            hangUpError = err;
          }
          delete callSession[currentCounterparty];
          callingNow = false;
        } else if(callSession[currentCounterparty]) {
          try{
            var hangUpResult = callSession[currentCounterparty].hangup();
          } catch(err) {
            hangUpError = err;
          }
        }
        if(hangUpResult == -1 || !callSession[currentCounterparty] || hangUpError){
          callingIcon = false;
          chrome.browserAction.setIcon({'path': "assets/icons/icon.png"});
          callingVia = null;
          delete callSession[currentCounterparty];
          callingNow = false;
          stopCallingtone();
          if(secondsCounter > 0 && isAnswered){
            jQuery.post(
              'https://www.interbooker.com/admin/voip3/saveHistory.php',
              {historyID: callId[from], timeSec: secondsCounter, extension: chrome.extension.getURL('/')}
            );
          }
          counting = false;
          secondsCounter = 0;
          isAnswered = false;
        }
      } else if(e.type == 'i_new_call'){
        //Starting new incoming call session
        newIncomingCall(e);
      }
    }
  },
  loginListener: { events: '*', listener: function(e){
			console.info('LOGIN session event = ' + e.type);
			if(e.type == 'connected'){
				status = 'connected';
        online();
        chrome.browserAction.setIcon({'path': "assets/icons/icon.png"});
			} else if(e.type == 'terminated' && (!restartedAt || restartedAt < Date.now() - 1500)){
        restartedAt = Date.now();
        chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
        setTimeout(createStack, 1000, agent);
      }
	}},
}
