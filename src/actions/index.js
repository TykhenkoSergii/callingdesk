import axios from 'axios';
export const LOAD_NUMBERS = 'LOAD_NUMBERS';
export const CHANGE_WINDOW = 'CHANGE_WINDOW';
export const LOAD_AGENT = 'LOAD_AGENT';
export const LOAD_HISTORY = 'LOAD_HISTORY';
export const CREATE_SESSION = 'CREATE_SESSION';
export const CALLED_NUMBER = 'CALLED_NUMBER';
export const CLEAR_CALLED_NUMBER = 'CLEAR_CALLED_NUMBER';
export const MICROPHONE_ERROR = 'MICROPHONE_ERROR';
export const CALLED_AGENTS = 'CALLED_AGENTS';
export const ALL_CONTACTS = 'ALL_CONTACTS';
export const SAVED_BOOKING = 'SAVED_BOOKING';
export const SOUND_STATUS = 'SOUND_STATUS';
export const ONLINE_AGENTS = 'ONLINE_AGENTS';
export const CHECK_EMAIL = 'CHECK_EMAIL';
export const HISTORY_CHANGED = 'HISTORY_CHANGED';
export const NEW_ACCOUNT_INFO = 'NEW_ACCOUNT_INFO';
export const LOAD_CALLBACKS = 'LOAD_CALLBACKS';
export const LOAD_CONFERENCE_PARTICIPANTS = 'LOAD_CONFERENCE_PARTICIPANTS';
export const CALLBACK_CHANGED = 'CALLBACK_CHANGED';
export const COUNTERPARTY_ACCOUNT = 'COUNTERPARTY_ACCOUNT';
export const NEW_COUNTERPARTY_INFO = 'COUNTERPARTY_ACCOUNT';

export const ACCOUNT_INFO = 'ACCOUNT_INFO';
export const CALLING_INNER = 'CALLING_INNER';
export const ON_CHOOSING_COUNTERPARTY = 'ON_CHOOSING_COUNTERPARTY';

export function getHistory(offset, counterparty = null){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/getHistory.php', {withCredentials: true, params: {offset, counterparty, extension: chrome.extension.getURL('/')}}).then(response => {
      chrome.extension.sendMessage({key: 'loadHistory', callHistory: response.data});
      return dispatch(loadHistory(response.data, offset, counterparty));
    }).then(err => {
      return;
    });
  }
}

export function getCallBacks(){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/getCallback.php', {withCredentials: true, params: {extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(loadCallBacks(response.data));
    }).then(err => {
      return;
    });
  }
}

export function getContactInfo(number){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/accountInfo.php', {withCredentials: true, params: {number, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(contactInfo(response.data));
    }).then(err => {
      return;
    });
  }
}

export function callingInnerNumber(number, internalName){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/accountInfo.php', {withCredentials: true, params: {number, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(onCallingInnerNumber(response.data, internalName));
    }).then(err => {
      return;
    });
  }
}

export function counterpartyInfo(number){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/accountInfo.php', {withCredentials: true, params: {number, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(onCounterpartyInfo(response.data));
    }).then(err => {
      return;
    });
  }
}

export function onCalledAgents(number){
  return dispatch => {
    dispatch(cleanCalledAgents());
    return axios.get('https://www.interbooker.com/admin/voip3/getActiveAgents.php', {withCredentials: true, params: {number, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(loadCalledAgents(response.data));
    }).then(err => {
      return;
    });
  }
}

export function onAllContacts(filter){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/searchContacts.php', {withCredentials: true, params: {filter, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(loadAllContacts(response.data));
    }).then(err => {
      return;
    });
  }
}

export function searchByContact(contact_id){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/searchContacts.php', {withCredentials: true, params: {filter: '', contact_id, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(loadAllContacts(response.data));
    }).then(err => {
      return;
    });
  }
}

export function onSaveBooking(booking_id, publicid){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/saveBooking.php', {withCredentials: true, params: {booking_id, publicid, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(onSavedBooking(response.data, booking_id));
    }).then(err => {
      return;
    });
  }
}

export function onCheckEmail(email){
  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/checkEmail.php', {withCredentials: true, params: {email, extension: chrome.extension.getURL('/')}}).then(response => {
      return dispatch(checkEmail(response.data));
    }).then(err => {
      return;
    });
  }
}

export function saveContactCall(account, number, booking_id = 0){

  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/createAccount.php',
    {withCredentials: true, params: {guid: account.guid, displayname: account.displayname, ownerid: account.ownerid, number, booking_id, extension: chrome.extension.getURL('/')}}).then((response) => {
      dispatch({type: NEW_ACCOUNT_INFO, payload: response.data[0]});
      dispatch({type: HISTORY_CHANGED, payload: response.data});
    });
  }
}

export function saveContactHistory(account, number, history_id){

  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/createAccount.php',
    {withCredentials: true, params: {guid: account.guid, displayname: account.displayname, ownerid: account.ownerid, number, history_id, extension: chrome.extension.getURL('/')}}).then((response) => {
      dispatch({type: NEW_ACCOUNT_INFO, payload: response.data[0]});
      dispatch({type: HISTORY_CHANGED, payload: response.data});
    });
  }
}

export function saveContactCallback(account, number, callback_id, booking_id){
  const {guid, displayname, ownerid, img} = account;

  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/createAccount.php',
    {withCredentials: true, params: {guid, displayname, ownerid, img, number, callback_id, booking_id, extension: chrome.extension.getURL('/')}}).then((response) => {
      dispatch({type: CALLBACK_CHANGED, payload: response.data});
    });
  }
}

export function createContactCall(number, name, mail = '', history_id = '', booking_id = 0){

  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/createAccount.php', {withCredentials: true, params: {mail, number, name, history_id, booking_id, extension: chrome.extension.getURL('/')}}).then((response) => {
      dispatch({type: NEW_ACCOUNT_INFO, payload: response.data[0]});
      dispatch({type: HISTORY_CHANGED, payload: response.data});
    });
  }
}

export function createContactHistory(number, name, mail = '', history_id = '', booking_id = 0){

  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/createAccount.php', {withCredentials: true, params: {mail, number, name, history_id, booking_id, extension: chrome.extension.getURL('/')}}).then((response) => {
      dispatch({type: NEW_ACCOUNT_INFO, payload: response.data[0]});
      dispatch({type: HISTORY_CHANGED, payload: response.data});
    });
  }
}

export function createContactCallback(number, name, mail = '', history_id = '', booking_id = 0){

  return dispatch => {
    return axios.get('https://www.interbooker.com/admin/voip3/createAccount.php', {withCredentials: true, params: {mail, number, name, history_id, booking_id, extension: chrome.extension.getURL('/')}}).then((response) => {
      dispatch({type: NEW_ACCOUNT_INFO, payload: response.data[0]});
      dispatch({type: CALLBACK_CHANGED, payload: response.data});
    });
  }
}

const loadAllContacts = (contacts) => {
  return{
    type: ALL_CONTACTS,
    payload: contacts
  }
}

const contactInfo = (contact) => {
  return{
    type: ACCOUNT_INFO,
    payload: contact
  }
}

const onSavedBooking = (response, booking_id) => {
  return{
    type: SAVED_BOOKING,
    payload: {response, booking_id}
  }
}

const onCallingInnerNumber = (response, innerName) => {
  return{
    type: CALLING_INNER,
    payload: {response, innerName}
  }
}

const onCounterpartyInfo = (response) => {
  return{
    type: COUNTERPARTY_ACCOUNT,
    payload: response
  }
}

export const onChoosingCounterparty = () => {
  return{
    type: ON_CHOOSING_COUNTERPARTY,
    payload: null
  }
}

const loadCalledAgents = (agents) => {
  return{
    type: CALLED_AGENTS,
    payload: agents
  }
}

const cleanCalledAgents = () => {
  return{
    type: CALLED_AGENTS,
    payload: []
  }
}

export const loadHistory = (history, offset = 0, counterparty = null) => {
  return{
    type: LOAD_HISTORY,
    payload: {history, offset, counterparty}
  }
}

export const loadCallBacks = (callBacks) => {
  return{
    type: LOAD_CALLBACKS,
    payload: callBacks
  }
}

export const changeWindow = (currentWindow, status = 'connected') => {
  return{
    type: CHANGE_WINDOW,
    payload: {currentWindow, status}
  }
}

export const loadAgent = (agent) => {
  return{
    type: LOAD_AGENT,
    payload: agent
  }
}

export const createSession = (session) => {
  return{
    type: CREATE_SESSION,
    payload: session
  }
}

export const setCalledNumbers = (counterparty, identity, direction) => {
  return{
    type: CALLED_NUMBER,
    payload: {counterparty, identity, direction}
  }
}

export const clearCalledNumbers = () => {
  return{
    type: CLEAR_CALLED_NUMBER
  }
}

export const setMicrophoneError = (error) => {
  return{
    type: MICROPHONE_ERROR,
    payload: error
  }
}

export const changeSoundStatus = (soundStatus) => {
  return{
    type: SOUND_STATUS,
    payload: soundStatus
  }
}

export const saveOnlineAgents = (onlineAgents) => {
  return{
    type: ONLINE_AGENTS,
    payload: onlineAgents
  }
}

export const checkEmail = (emailResult) => {
  return{
    type: CHECK_EMAIL,
    payload: emailResult
  }
}

export const loadConferenceParticipants = (conferenceParticipants) => {
  return{
    type: LOAD_CONFERENCE_PARTICIPANTS,
    payload: conferenceParticipants
  }
}
