import React, { Component } from 'react';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import _ from 'lodash';
import {getContactInfo, setCalledNumbers, getCallBacks, saveContactCallback, createContactCallback, onCheckEmail, counterpartyInfo} from '../actions';

class CallBack extends Component{

  constructor(props){
    super(props);

    this.editAccountObj = {};
    this.state = {
      emailError: false,
      checkEmail: '',
      openedPopup: false,
      startCheckingEmail: false,
      emailTimeout: null
    }
  }

  componentDidMount(){

    this.props.getCallBacks();

  }

  componentWillReceiveProps(nextProps){
    //closing opened popups
    if(nextProps.trigeredEvent && !nextProps.trigeredEvent.target.closest('.createAccountPopup') && !nextProps.trigeredEvent.target.closest('.fa-edit') && this.state.openedPopup !== false){
      this.editAccountObj[this.state.openedPopup].isOpen = false;
      this.setState({
        openedPopup: false
      });
    }
    this.setState({
      startCheckingEmail: false
    });
  }

  call(callBack){

    const counterparty = {
      contact_id: callBack.counterparty_id,
      guid: callBack.counterparty_guid,
      firstName: callBack.counterparty_name,
      img: callBack.counterparty_icon,
      number: callBack.counterparty_lastaccount_number,
      flag: callBack.counterparty_lastaccount_flag,
      isBooking: callBack.subject
    };

    const identity = {
      ownerid: callBack.identity_id,
      contactguid: callBack.identity_guid,
      name: callBack.identity_name,
      account_img: callBack.identity_icon,
      phonenumber: callBack.identity_lastaccount_number,
      countryFlag: callBack.identity_lastaccount_flag
    };

    chrome.extension.sendMessage({key: 'initCall', identity, counterparty});
    this.props.counterpartyInfo(counterparty);
    this.props.setCalledNumbers(this.props.agent.phonenumber, callBack.identity_lastaccount_number, callBack.counterparty_lastaccount_number);
  }

  openEditBox(i, contactName = ''){
    if(this.state.openedPopup !== false && this.state.openedPopup !== i && this.editAccountObj[this.state.openedPopup]){
      this.editAccountObj[this.state.openedPopup].isOpen = false;
    }

    if(!this.editAccountObj[i].isOpen){
      this.editAccountObj[i].isOpen = true;
      this.setState({
        openedPopup: i
      });
    } else {
      this.editAccountObj[i].isOpen = false;
      this.setState({
        openedPopup: false
      });
    }

    this.setState({contactName});
  }
  checkEmail(input){

    if(input.length < 3 || (input.indexOf('@') < 1 || input.indexOf('.') < 1 ||
      (input.indexOf('.') > 0 && input.lastIndexOf('.') < input.indexOf('@') + 1) || input.lastIndexOf('.') == input.length - 1) && isNaN(+input)){
      this.setState({
        emailError: true,
        checkEmail: input
      });
    } else if(input.length > 0) {
      this.setState({
        emailError: false,
        checkEmail: input
      });

      const currentState = this;
      let emailTimeout = this.state.emailTimeout;
      clearTimeout(emailTimeout);

      emailTimeout = setTimeout(function(){
          currentState.props.onCheckEmail(input);
          currentState.setState({
            startCheckingEmail: true
          });
      }, 500);

      this.setState({
        emailTimeout
      });

    } else {
      this.setState({
        emailError: false,
        checkEmail: ''
      });
    }
  }

  emailCheckboxChange(){
    this.setState({
      emailCheckbox: !this.state.emailCheckbox
    });
  }

  onChangeName(name){
    this.setState({
      contactName: name
    })
  }

  createAccount(e, number, callback_id, i){
    e.preventDefault();
    this.editAccountObj[i].isOpen = false;
    this.setState({
      openedPopup: false
    });
    if(this.state.checkEmail.length > 0 && this.props.checkEmail.publicid == this.state.checkEmail && this.props.checkEmail.displayname){
      if(!isNaN(+this.state.checkEmail)){
        this.props.saveContactCallback(this.props.checkEmail, number, callback_id, this.state.checkEmail);
      } else {
        this.props.saveContactCallback(this.props.checkEmail, number, callback_id, 0);
      }
    } else if((this.props.checkEmail.publicid == this.state.checkEmail && !this.props.checkEmail.displayname && this.state.contactName.length > 0) ||
      (this.state.emailCheckbox && !this.props.checkEmail.displayname && this.state.contactName.length > 0)){
      this.props.createContactCallback(number, this.state.contactName, this.state.checkEmail, callback_id);
    }
  }

  render(){
    const {callBacks, voipqNumbers, checkEmail} = this.props;

    if(callBacks != null){
      return(
        <div>
          <div className="single-history single-history-header">
            <div>Scheduled</div>
            <div>Counterparty</div>
            <div>Identity</div>
          </div>
          {_.map(callBacks, (callBack, i) => {
            return(
              <div key={callBack.id} className="single-history">
                <div className="history-time">
                  {callBack.last_direction == 1 ?

                    <img className="answered-img" src="./assets/icons/mis.png"/>

                  : callBack.last_direction == 2 &&

                    <img className="answered-img" src="./assets/icons/out-mis.png"/>

                  }
                  <span>{callBack.scheduled}</span><br/>
                </div>
                <div className="history-from">
                  {callBack.counterparty_id && callBack.counterparty_id != 0 &&
                    <div>
                      <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+callBack.counterparty_id} target="blank" style={{marginBottom: '3px', display: 'inline-block'}}>{callBack.counterparty_name}<br /></a>

                    </div>
                  }
                  <div className="counterparty-number" style={{marginTop: '5px'}}>
                    {callBack.counterparty_lastaccount_number == 'anonymous' ?
                      <span className="anonymous-flag">?</span>
                    : callBack.counterparty_lastaccount_number && callBack.counterparty_lastaccount_number.length < 7 ?
                      <img className="flag" src={"assets/img/globe2.png"} style={{width: 'auto'}}/>
                    :
                      <img className="flag" src={"http://www.geonames.org/flags/x/"+callBack.counterparty_lastaccount_flag+".gif"} />
                    }
                    <div>
                      {callBack.counterparty_lastaccount_number}
                      {(!callBack.counterparty_id || callBack.counterparty_id == 0) && voipqNumbers.indexOf(callBack.counterparty_lastaccount_number) == -1 && callBack.counterparty_lastaccount_number != 'anonymous' &&
                        <span style={{position: "relative"}}>
                          <i className="fas fa-edit" onClick={() => this.openEditBox(i)}></i>
                          <div ref={el => {this.editAccountObj[i] = el}} className="createAccountPopup"
                            style={this.editAccountObj[i] && this.editAccountObj[i].isOpen ?
                              {height: (this.state.emailCheckbox || (!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && this.state.checkEmail.length > 1 && !this.state.emailError))
                               && !checkEmail.displayname && checkEmail.publicid == this.state.checkEmail ? "165px" :
                               this.state.emailCheckbox || (!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && this.state.checkEmail.length > 1 && !this.state.emailError) ? "160px" :
                               checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !this.state.emailError ? "max-content" : "110px", width: "250px", padding: "10px"} : {}
                            }>
                            <form onSubmit={(e) => this.createAccount(e, callBack.counterparty_lastaccount_number, callBack.id, i)} method="post">
                              <div>
                                <div style={{position: "relative"}}>
                                  <input onChange={(e) => this.checkEmail(e.target.value)} type="text" value={!this.state.emailCheckbox ? this.state.checkEmail : ''}
                                    placeholder={!this.state.emailCheckbox ? 'Type email or booking#...' : ''}
                                    style={this.state.emailError ? {borderBottom: '1px solid #ff0000', width: '90%', color: '#ff0000'} : {borderBottom: '1px solid #000', width: '90%'}} disabled={this.state.emailCheckbox} />
                                  <img src="./assets/img/loader-sm.gif" className="loader-sm" style={this.state.startCheckingEmail ? {display: 'block'} : {}}/>
                                  {!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !isNaN(+this.state.checkEmail) ?
                                    <div style={{fontSize: '10px', position: 'absolute', bottom: '1px', left: '10px', color: 'red'}}>Unknown booking#</div>
                                  : !checkEmail.displayname && checkEmail.publicid == this.state.checkEmail &&
                                    <div style={{fontSize: '10px', position: 'absolute', bottom: '1px', left: '10px', color: 'red'}}>Unknown email</div>
                                  }
                                </div>
                                <div style={{paddingTop: '10px', width: 'max-content'}}>
                                  <input id="email_checkbox" type="checkbox" value={this.state.emailCheckbox}
                                    onChange={() => this.emailCheckboxChange()} checked={this.state.emailCheckbox} style={{verticalAlign: 'middle'}}/>
                                  <lable for="email_checkbox">{"Both email and booking# are unknown"}</lable>
                                </div>
                              </div>
                              <div>
                                {this.state.emailCheckbox || (!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && this.state.checkEmail.length > 1 && !this.state.emailError) ?
                                  <div>
                                    <input onChange={(e) => this.onChangeName(e.target.value)} type="text" placeholder="name" value={this.state.contactName} style={{borderBottom: '1px solid #000', width: '90%'}}/>
                                    <div style={{color: 'red', marginTop: '5px', fontWeight: '600'}}>Name is required</div>
                                  </div>
                                : checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !this.state.emailError &&
                                  <div>
                                    <input type="text" value={checkEmail.displayname} style={{borderBottom: '1px solid #000', width: '90%'}} disabled={true}/>
                                    {isNaN(+this.state.checkEmail) ?
                                      <div style={{marginTop: '5px', fontWeight: '600'}}>Email Found</div>
                                    :
                                      <div style={{marginTop: '5px', fontWeight: '600'}}>Booking Found</div>
                                    }
                                    <a href={'https://www.interbooker.com/admin/contact.php?IdContacts='+checkEmail.ownerid} target="_blank">
                                      <img src={checkEmail.img} style={{marginTop: '10px', width: '100px', borderRadius: '10px', boxShadow: '2px 2px 10px rgba(0,0,0,0.5)'}} />
                                    </a>
                                  </div>
                                }
                              </div>
                              <div style={{marginTop: '5px', textAlign: 'center'}}>
                                <button className="button button-blue">Save</button>
                              </div>
                            </form>
                          </div>
                        </span>
                      }
                      {callBack.subject && Number.isInteger(+callBack.subject) && callBack.subject != 0 ?
                        <div className="booking_box">
                          <span>Booking id:
                            <a href={"https://www.interbooker.com/admin/booking.php?bkid="+callBack.subject} target="blank"> {callBack.subject}</a>
                          </span>
                        </div>
                      : callBack.subject && callBack.subject != 0 &&
                        <div className="booking_box">
                          <span>Subject: {callBack.subject}</span>
                        </div>
                      }
                    </div>
                    <button onClick={() => this.call(callBack)} className="call-btn"><i className="fas fa-phone"></i></button>
                  </div>
                </div>
                <div className="history-to">
                  <div className="history-identity">
                    <span>
                      <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+callBack.identity_id} target="blank">
                        {callBack.identity_name}
                      </a>
                      <br />
                    </span>
                    <div style={{marginTop: '5px'}}>
                      <img className="flag" src={"http://www.geonames.org/flags/x/"+callBack.identity_lastaccount_flag+".gif"} />
                      {callBack.identity_lastaccount_number}
                    </div>
                  </div>
                  <br />
                </div>
              </div>
            );
          })}
        </div>
      );
    } else {
      return <div>There is no any scheduled callbacks</div>;
    }
  }
}

function mapStateToProps(state){
    return{
      agent: state.agentReducer.agent,
      callBacks: state.callBacksReducer,
      voipqNumbers: state.voipQReducer,
      checkEmail: state.checkEmailReducer
    }
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getContactInfo,
    setCalledNumbers,
    getCallBacks,
    saveContactCallback,
    createContactCallback,
    onCheckEmail,
    counterpartyInfo
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(CallBack);
