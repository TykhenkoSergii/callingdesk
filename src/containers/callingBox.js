import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {changeWindow, onCalledAgents, getHistory, loadHistory, onSaveBooking} from '../actions';
import HistoryBlock from './components/historyBlock';
import DialPad from './components/dialPad.jsx';
import Contact from './components/contact.jsx';

class CallingBox extends Component{

  constructor(props){
    super(props);
    this.state={
      counterparty: null,
      offset: 0,
      historyHeight: 0,
      transferStyle:{
        height: '0'
      },
      timer: {
        hours: 0,
        minutes: 0,
        seconds: 0
      },
      booking_id: '',
      isDialPadOpened: false
    }
  }

  onHangUp(){
    chrome.extension.sendMessage({key: 'hangUp'});
  }

  onAccept(){
    chrome.extension.sendMessage({key: 'acceptCall'});
    this.setState(timer: {
      hours: 0,
      minutes: 0,
      seconds: 0
    });
    this.timer(true);
  }

  onHold(){
    chrome.extension.sendMessage({key: 'holdCall'});
  }

  onResume(){
    chrome.extension.sendMessage({key: 'resumeCall'});
  }

  onTransferClick(){
    this.setState({
      transferStyle: {
        height: '100%'
      }
    });
  }

  onTransNumChange(e){
    this.setState({
      transferNumber: e.target.value
    });
  }

  onTransferSubmit(e){
    e.preventDefault();
    chrome.extension.sendMessage({key: 'transferCall', number: this.state.transferNumber});
  }

  componentDidUpdate(){
    const { counterparty}  = this.props.calledNumber;

    if(counterparty && this.props.historyResult.counterparty != counterparty &&
      (this.props.currentWindow == 'callingBoxIn' || this.props.currentWindow == 'onHoldIn' || this.props.currentWindow == 'callingBoxOut' || this.props.currentWindow == 'onHoldOut')){
      if(this.state.counterparty != counterparty){
        this.setState({counterparty, offset: 0});
      }
      this.props.getHistory(0, counterparty);

    }

    let historyHeight = window.innerHeight - this.callingMain.offsetTop - this.callingMain.clientHeight - 5;
    if(this.callingTenant)
      historyHeight = historyHeight - this.callingTenant.clientHeight - 24;

    if(historyHeight != this.state.historyHeight)
      this.setState({historyHeight});

  }

  onHistoryScroll(e){
    const { counterparty}  = this.props.calledNumber;

    if(e.target.scrollTop > this.historyWraper.clientHeight - this.historyDiv.clientHeight - 200 && this.state.offset == this.props.historyResult.offset ){
      const newOffset = this.state.offset + 20;
      this.props.getHistory(newOffset, counterparty);
      this.setState({
        offset: newOffset
      });
    }
  }

  timer(start = false){
    const currentState = this;

    clearInterval(this.timerInterval);

    if(start){
      currentState.setState({
        timer: {
          hours: 0,
          minutes: 0,
          seconds: 0
        }
      });
    }
    this.timerInterval = setInterval(function(){
      if(currentState.state.timer.seconds < 59){
        currentState.setState({
          timer:{
            hours: currentState.state.timer.hours,
            minutes: currentState.state.timer.minutes,
            seconds: currentState.state.timer.seconds + 1,
          }
        });
      } else {
        if(currentState.state.timer.minutes < 59){
          currentState.setState({
            timer:{
              hours: currentState.state.timer.hours,
              minutes: currentState.state.timer.minutes + 1,
              seconds: 0,
            }
          });
        } else {
          currentState.setState({
            timer:{
              hours: currentState.state.timer.hours + 1,
              minutes: 0,
              seconds: 0,
            }
          });
        }
      }
    }, 1000);
  }

  stopTimer(){
    clearInterval(this.timerInterval);
  }

  componentDidMount(){
    const currentState = this;
    chrome.runtime.onMessage.addListener(function(request, sender, callback) {
      switch(request.key) {
        case 'runTimer':
          currentState.setState({
            timer: request.timer
          });
          if(request.timer.seconds > 0 || request.timer.minutes > 0 || request.timer.hours > 0 ){
            currentState.timer();
          } else {
            currentState.stopTimer();
          }
          break;
        case 'startTimer':
          currentState.timer(true);
          break;
        default:
          return null;
      }
    });
  }

  onBookingChange(e){
    this.setState({
      booking_id: e.target.value
    })
  }

  saveBooking(){
    const {calledNumber, agent, onSaveBooking} = this.props;

    const counterpartyNumber = calledNumber.from != agent.phonenumber ? calledNumber.from : calledNumber.to;

    onSaveBooking(this.state.booking_id, counterpartyNumber);
  }

  openKeypad(){
    const currentKeypad = this.state.isDialPadOpened;
    this.setState({
      isDialPadOpened: !currentKeypad
    });
  }

  onConferenceClick(){
    chrome.extension.sendMessage({key: 'toConference', participant: this.props.counterparty});
  }

  render(){

    const {currentWindow, currentStatus, agent, calledNumber, counterparty, voipqNumbers, changeWindow, calledAgents, historyResult} = this.props;
      console.log({...agent})
        console.log(agent)
    return(
      <div className="calling-box">
        <div className="calling-buttons">
          <button onClick={() => this.onAccept()} className="call-btn"><i className="fas fa-phone"></i></button>

          {currentStatus == 'onHoldIn' || currentStatus == 'onHoldOut' ?
            <button onClick={() => this.onResume()} className="resume-btn">Resume</button>
          :
            <button onClick={() => this.onHold()} className="hold-btn">Hold</button>
          }
          <button onClick={() => {this.onConferenceClick()}} className="transfer-btn" style={{width: '115px'}}>Conference</button>
          <button onClick={() => {this.onTransferClick()}} className="transfer-btn">Transfer</button>
          <button onClick={() => this.onHangUp()} className="hangup-btn"><i className="fas fa-phone"></i></button>
          <div className="keypad-btn">
            <img onClick={() => this.openKeypad()} src="./assets/icons/keypad.png" />
            {this.state.isDialPadOpened &&
              <DialPad />
            }
          </div>
        </div>
        <div className="transfer-call" style={this.state.transferStyle}>
          <form onSubmit={(e)=>this.onTransferSubmit(e)}>
            <input type="text" onChange={(e)=>this.onTransNumChange(e)} placeholder="Transfer Number"/>
            <button className="transfer-btn">Transfer</button>
          </form>
        </div>
        <div className="calling-main" ref={(callingMain) => {this.callingMain = callingMain}}>
          <div className="single-history single-history-header">
            <div>Time</div>
            <div>Counterparty</div>
            <div>Identity</div>
          </div>
          <div className="calling-from">
            {this.state.timer.hours < 10 ? '0' + this.state.timer.hours : this.state.timer.hours}:
            {this.state.timer.minutes < 10 ? '0' + this.state.timer.minutes : this.state.timer.minutes}:
            {this.state.timer.seconds < 10 ? '0' + this.state.timer.seconds : this.state.timer.seconds}
          </div>
          <div className="calling-via">
            <div className="counterparty-number">

              <Contact voipqNumbers={voipqNumbers} contact={counterparty.flag ? counterparty : calledNumber.counterparty} />

              <div className="booking_box">
                {counterparty.booking_id && voipqNumbers.indexOf(calledNumber.counterparty) == -1 ?
                  <span>Booking id:
                    <a href={"https://www.interbooker.com/admin/booking.php?bkid="+counterparty.booking_id} target="blank"> {counterparty.booking_id}</a>
                  </span>
                : voipqNumbers.indexOf(calledNumber.counterparty) == -1 ?
                  <span>
                    No bookings
                  </span>
                :
                  <span></span>
                }
              </div>
            </div>
          </div>
          <div className="calling-to history-to">
            {calledNumber.identity != 'Internal' && agent.numbers_object && agent.numbers_object[calledNumber.identity] ?
              <div>
                <div className="history-identity">
                  <Contact voipqNumbers={voipqNumbers} contact={agent.numbers_object[calledNumber.identity]} />
                </div>
              </div>
            : calledNumber.identity == 'Internal' || calledNumber.identity == agent.phonenumber ?
              <span>
                Internal
              </span>
            :
              <span>
                Unknown number
              </span>
            }
            <br/>
            <div className="called-agent">
              {calledNumber.direction == 'out' ?
                <img className="answered-img" src="./assets/icons/out.png" />
              :
                <img className="answered-img" src="./assets/icons/inc.png" />
              }
              <div className="history-identity">
                <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+agent.contact_id} target="blank">
                  {agent.name}
                </a>
                <div>{agent.publicid}</div>
              </div>
            </div>

            {calledNumber.direction == 'in' &&
              <div className="called-agent">
                {_.map(calledAgents, (calledAgent, i) => {
                  if(calledAgent.identity_agent != this.props.agent.phonenumber){
                    return(
                      <div key={i}>
                        <img className="answered-img" src="./assets/icons/inc.png" />
                        <div className="history-identity" key={i}>
                          <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+calledAgent.agent_id} target="blank">
                            {calledAgent.agent_name}
                          </a>
                          <div>{calledAgent.identity_agent}</div>
                        </div>
                      </div>
                    );
                  }
                })}
              </div>
            }

          </div>
        </div>
        {agent.numbers_object && agent.numbers_object[calledNumber.identity] && (agent.numbers_object[calledNumber.identity].tenant_1 || agent.numbers_object[calledNumber.identity].tenant_2) &&

          <div className="calling-tenant" ref={callingTenant => this.callingTenant = callingTenant}>
            {'Good morning/afternoon/evening, this is ' + (agent.numbers_object[calledNumber.identity].name ? agent.numbers_object[calledNumber.identity].name + ' ' : '') + ', '}
            {agent.numbers_object[calledNumber.identity].tenant_1 ? <span>{agent.numbers_object[calledNumber.identity].tenant_1}</span> : ''}
            {agent.numbers_object[calledNumber.identity].tenant_1 && agent.numbers_object[calledNumber.identity].tenant_2 ? ' & ' : ''}
            {agent.numbers_object[calledNumber.identity].tenant_2 ? <span>{agent.numbers_object[calledNumber.identity].tenant_2}</span> : ''}
            {' are busy at work, but how can I help you?'}
          </div>

        }
        {historyResult.counterparty != this.state.counterparty ?
          <div style={{textAlign: 'center'}}>
            <img src="/assets/icons/ajax-loader.gif" style={{width: '40px'}} />
          </div>
        :
          <div className='history' onScroll={(e) => this.onHistoryScroll(e)} ref={(historyDiv) => {this.historyDiv = historyDiv}} style={{height: this.state.historyHeight}}>
            <div className="history-wraper" ref={(historyWraper) => {this.historyWraper = historyWraper}} style={{marginTop: "20px"}}>
              <HistoryBlock />
            </div>
          </div>
        }

      </div>
    );
  }

}

function mapStateToProps(state){
    return{
      currentWindow: state.windowReducer.window,
      currentStatus: state.windowReducer.status,
      agent: state.agentReducer.agent,
      calledNumber: state.calledNumberReducer,
      calledAgents: state.calledAgentsReducer,
      historyResult: state.historyReducer,
      voipqNumbers: state.voipQReducer,
      counterparty: state.counterpartyReducer
    }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    changeWindow,
    onCalledAgents,
    getHistory,
    loadHistory,
    onSaveBooking,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CallingBox);
