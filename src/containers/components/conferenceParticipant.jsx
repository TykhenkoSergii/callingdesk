import React from 'react';

const styles = {
  hangupBtn: {color: 'red'}
}

function throwParticipant(number){
  chrome.extension.sendMessage({key: 'throwParticipant', number});
}

export default ({conferenceParticipant}) => (
  <div className="conferenceParticipant" key={conferenceParticipant.number}>
    {conferenceParticipant.img ?
      <img className="avatar" src={conferenceParticipant.img} />
    :
      <img className="avatar" src='https://www.interbooker.com/admin/css/no-user.png' />
    }
    <div>
      <div>
        {conferenceParticipant.number}
        {conferenceParticipant.status == 'calling' ?
          <img className="status-icon" src="./assets/icons/ringGreen.png" />
        :
          <img className="status-icon" src="./assets/icons/out.png" />
        }
      </div>
      <div>{conferenceParticipant.name ? conferenceParticipant.name : ''}{conferenceParticipant.displayname ? '('+conferenceParticipant.displayname+')' : ''}</div>
    </div>
    <div className="hangup-participant" onClick={() => throwParticipant(conferenceParticipant.number)}>
      <i class="fas fa-phone" style={styles.hangupBtn}></i>
    </div>
  </div>
);
