import React from 'react';

const renderHTML = (rawHTML: string) => React.createElement("span", { dangerouslySetInnerHTML: { __html: rawHTML } });


export default ({contact, voipqNumbers}) => (
  <div>
    {contact &&
      <div>
        {voipqNumbers.indexOf(contact.publicid) != -1 ?
          <div style={{marginBottom: '5px'}}>
            VoipQ Number
          </div>
        : contact.contact_id != 'inner' && contact.name && contact.name != contact.publicid ?
          <div style={{marginBottom: '5px'}}>
            <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+contact.contact_id} target="blank">
              {renderHTML(contact.name)}
            </a>
          </div>
        : contact.contact_id != 'inner' ?
          <div style={{marginBottom: '5px'}}>
            <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+contact.contact_id} target="blank">
              {contact.firstName ? renderHTML(contact.firstName + ' ') : ''}
              {contact.middleName ? renderHTML(contact.middleName + ' ') : ''}
              {contact.lastName ? renderHTML(contact.lastName) : ''}
            </a>
          </div>
        :
          <div style={{marginBottom: '5px'}}>
            Inner
          </div>
        }
        <div>
          {contact.publicid && contact.publicid.length < 7 ?

           <img className="flag" src={"assets/img/globe2.png"} style={{width: 'auto'}}/>

          : contact.flag && contact.publicid != 'anonymous' ?

            <img className="flag" src={"http://www.geonames.org/flags/x/"+contact.flag+".gif"} />

          :

            <span className="anonymous-flag">?</span>

          }
          {contact.publicid ?
            <span>{contact.publicid}</span>
          :
            <span>{contact}</span>
          }
        </div>
      </div>
    }
  </div>
);
