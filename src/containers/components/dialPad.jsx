import React from 'react';

export default () => {

  const onDialClick = dialKey => chrome.extension.sendMessage({key: 'dtmfclick', dialKey});

  return(
    <div className="dialPad">
      <div className="dialPadRow">
        <div onClick={() => onDialClick('1')} className="dialPadCell">
          <span>1</span>
        </div>
        <div onClick={() => onDialClick('2')} className="dialPadCell">
          <span>2</span>
        </div>
        <div onClick={() => onDialClick('3')} className="dialPadCell">
          <span>3</span>
        </div>
      </div>
      <div className="dialPadRow">
        <div onClick={() => onDialClick('4')} className="dialPadCell">
          <span>4</span>
        </div>
        <div onClick={() => onDialClick('5')} className="dialPadCell">
          <span>5</span>
        </div>
        <div onClick={() => onDialClick('6')} className="dialPadCell">
          <span>6</span>
        </div>
      </div>
      <div className="dialPadRow">
        <div onClick={() => onDialClick('7')} className="dialPadCell">
          <span>7</span>
        </div>
        <div onClick={() => onDialClick('8')} className="dialPadCell">
          <span>8</span>
        </div>
        <div onClick={() => onDialClick('9')} className="dialPadCell">
          <span>9</span>
        </div>
      </div>
      <div className="dialPadRow">
        <div onClick={() => onDialClick('*')} className="dialPadCell">
          <span>*</span>
        </div>
        <div onClick={() => onDialClick('0')} className="dialPadCell">
          <span>0</span>
        </div>
        <div onClick={() => onDialClick('#')} className="dialPadCell">
          <span>#</span>
        </div>
      </div>
    </div>
  );
}
