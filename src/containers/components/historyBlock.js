import React, { Component } from 'react';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import _ from 'lodash';
import {getContactInfo, setCalledNumbers, onCheckEmail, saveContactHistory, counterpartyInfo, createContactCall} from '../../actions';
import Contact from './contact.jsx';

class HistoryBlock extends Component{

  constructor(props){
    super(props);
    this.editAccountObj = {};
    this.state={
      emailError: false,
      checkEmail: '',
      openedPopup: false,
      startCheckingEmail: false
    }
  }

  componentWillReceiveProps(nextProps){
    //closing opened popups
    if(nextProps.trigeredEvent && !nextProps.trigeredEvent.target.closest('.createAccountPopup') && !nextProps.trigeredEvent.target.closest('.fa-edit') && this.state.openedPopup !== false){
      this.editAccountObj[this.state.openedPopup].isOpen = false;
      this.setState({
        openedPopup: false
      });
    }
    this.setState({
      startCheckingEmail: false
    });
  }

  calledAgents(historyArr, i, step = 1){
    if(historyArr[i+step] && historyArr[i].callhistory_id == historyArr[i+step].callhistory_id){
      historyArr[i].identity_agent = historyArr[i].identity_agent + '<br>' + historyArr[i+step].identity_agent;
      step++;
      return this.calledAgents(historyArr, i, step)
    } else {
      return historyArr[i].identity_agent;
    }
  }

  calling(identityNumber, counterpartyNumber){

    const {counterpartyInfo, setCalledNumbers, agent} = this.props;
    const counterparty = {
      phonenumber: counterpartyNumber,
      number: counterpartyNumber
    }

    const identity = {
      phonenumber: identityNumber ? identityNumber : 'inner',
      number: identityNumber ? identityNumber : 'inner'
    }

    chrome.extension.sendMessage({key: 'initCall', identity, counterparty});
    counterpartyInfo(counterpartyNumber);
    setCalledNumbers(counterpartyNumber, identityNumber, 'out');

  }

  openEditBox(i, contactName = ''){
    if(this.state.openedPopup !== false && this.state.openedPopup !== i && this.editAccountObj[this.state.openedPopup]){
      this.editAccountObj[this.state.openedPopup].isOpen = false;
    }

    if(!this.editAccountObj[i].isOpen){
      this.editAccountObj[i].isOpen = true;
      this.setState({
        openedPopup: i
      });
    } else {
      this.editAccountObj[i].isOpen = false;
      this.setState({
        openedPopup: false
      });
    }

    this.setState({contactName});
  }

  checkEmail(input){

    if(input.length < 3 || (input.indexOf('@') < 1 || input.indexOf('.') < 1 ||
      (input.indexOf('.') > 0 && input.lastIndexOf('.') < input.indexOf('@') + 1) || input.lastIndexOf('.') == input.length - 1) && isNaN(+input)){
      this.setState({
        emailError: true,
        checkEmail: input
      });
    } else if(input.length > 0) {
      this.setState({
        emailError: false,
        checkEmail: input
      });

      const currentState = this;
      let emailTimeout = this.state.emailTimeout;
      clearTimeout(emailTimeout);

      emailTimeout = setTimeout(function(){
          currentState.props.onCheckEmail(input);
          currentState.setState({
            startCheckingEmail: true
          });
      }, 500);

      this.setState({
        emailTimeout
      });

    } else {
      this.setState({
        emailError: false,
        checkEmail: ''
      });
    }
  }

  emailCheckboxChange(){
    this.setState({
      emailCheckbox: !this.state.emailCheckbox
    });
  }

  onChangeName(name){
    this.setState({
      contactName: name
    })
  }

  createAccount(e, number, history_id, i){
    e.preventDefault();
    this.editAccountObj[i].isOpen = false;
    this.setState({
      openedPopup: false
    });
    if(this.state.checkEmail.length > 0 && this.props.checkEmail.publicid == this.state.checkEmail && this.props.checkEmail.displayname){
      this.props.saveContactHistory(this.props.checkEmail, number, history_id);
    } else if((this.props.checkEmail.publicid == this.state.checkEmail && !this.props.checkEmail.displayname && this.state.contactName.length > 0) ||
      (this.state.emailCheckbox && !this.props.checkEmail.displayname && this.state.contactName.length > 0)) {
      this.props.createContactCall(number, this.state.contactName, this.state.checkEmail, history_id);
    }
  }

  renderHTML(rawHTML: string){
    return React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });
  }

  render(){
    const {agent, historyResult, voipqNumbers, checkEmail} = this.props;

    if(historyResult.history != null){
      //reversing history
      let histories = Object.assign([], historyResult.history).reverse();
      histories = histories.filter(function (item) {
          return item !== undefined;
      });

      return(
        <div>
          {_.map(histories, (history, i) => {
            return(
              <div key={history.id} className="single-history">
                <div className="history-time">
                  {history.is_answered == 1 ?
                    history.outbound == 1 ?
                      <img className="answered-img" src="./assets/icons/out.png"/>
                    :
                      <img className="answered-img" src="./assets/icons/inc.png"/>

                  : history.is_answered == 2 ?
                    <img className="answered-img" src="./assets/icons/blocked.png"/>
                  :
                    history.outbound == 1 ?
                      <img className="answered-img" src="./assets/icons/out-mis.png"/>
                    :
                      <img className="answered-img" src="./assets/icons/mis.png"/>
                  }
                  <span>{history.start}</span><br/>
                  {history.duration > 0 ?
                    <span>({Math.floor(history.duration/360)}h:
                    {Math.floor((history.duration - Math.floor(history.duration/360)*360)/60)}m:
                    {history.duration - Math.floor(history.duration / 60) * 60}sec)</span>
                  :
                    <span></span>
                  }
                </div>
                <div className="history-from">
                  <div className="counterparty-number" style={{marginTop: '5px'}}>
                    <div style={{position: 'relative'}}>

                      <Contact voipqNumbers={voipqNumbers} contact={history.counterpartyUser} />

                      {(!history.contactId || history.contactId == 0) && voipqNumbers.indexOf(history.counterparty) == -1 && history.counterparty != 'anonymous' &&
                        <span style={{position: "absolute", top: '5px', right: '-20px'}}>
                          <i className="fas fa-edit" onClick={() => this.openEditBox(i)}></i>
                          <div ref={el => {this.editAccountObj[i] = el}} className="createAccountPopup"
                            style={this.editAccountObj[i] && this.editAccountObj[i].isOpen ?
                              {height: (this.state.emailCheckbox || (!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && this.state.checkEmail.length > 1 && !this.state.emailError))
                               && !checkEmail.displayname && checkEmail.publicid == this.state.checkEmail ? "165px" :
                               this.state.emailCheckbox || (!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && this.state.checkEmail.length > 1 && !this.state.emailError) ? "160px" :
                               checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !this.state.emailError ? "max-content" : "110px", width: "250px", padding: "10px"} : {}
                            }>
                            <form onSubmit={(e) => this.createAccount(e, history.counterparty, history.id, i)} method="post">
                              <div>
                                <div style={{position: "relative"}}>
                                  <input onChange={(e) => this.checkEmail(e.target.value)} type="text" value={!this.state.emailCheckbox ? this.state.checkEmail : ''}
                                    placeholder={!this.state.emailCheckbox ? 'Type email or booking#...' : ''}
                                    style={this.state.emailError ? {borderBottom: '1px solid #ff0000', width: '90%', color: '#ff0000'} : {borderBottom: '1px solid #000', width: '90%'}} disabled={this.state.emailCheckbox} />
                                  <img src="./assets/img/loader-sm.gif" className="loader-sm" style={this.state.startCheckingEmail ? {display: 'block'} : {}}/>
                                  {!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !isNaN(+this.state.checkEmail) ?
                                    <div style={{fontSize: '10px', position: 'absolute', bottom: '1px', left: '10px', color: 'red'}}>Unknown booking#</div>
                                  : !checkEmail.displayname && checkEmail.publicid == this.state.checkEmail &&
                                    <div style={{fontSize: '10px', position: 'absolute', bottom: '1px', left: '10px', color: 'red'}}>Unknown email</div>
                                  }
                                </div>
                                <div style={{paddingTop: '10px', width: 'max-content'}}>
                                  <input id="email_checkbox" type="checkbox" value={this.state.emailCheckbox}
                                    onChange={() => this.emailCheckboxChange()} checked={this.state.emailCheckbox} style={{verticalAlign: 'middle'}}/>
                                  <lable for="email_checkbox">{"Both email and booking# are unknown"}</lable>
                                </div>
                              </div>
                              <div>
                                {this.state.emailCheckbox || (!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && this.state.checkEmail.length > 1 && !this.state.emailError) ?
                                  <div>
                                    <input onChange={(e) => this.onChangeName(e.target.value)} type="text" placeholder="name" value={this.state.contactName} style={{borderBottom: '1px solid #000', width: '90%'}}/>
                                    <div style={{color: 'red', marginTop: '5px', fontWeight: '600'}}>Name is required</div>
                                  </div>
                                : checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !this.state.emailError &&
                                  <div>
                                    <input type="text" value={checkEmail.displayname} style={{borderBottom: '1px solid #000', width: '90%'}} disabled={true}/>
                                    {isNaN(+this.state.checkEmail) ?
                                      <div style={{marginTop: '5px', fontWeight: '600'}}>Email Found</div>
                                    :
                                      <div style={{marginTop: '5px', fontWeight: '600'}}>Booking Found</div>
                                    }
                                    <a href={'https://www.interbooker.com/admin/contact.php?IdContacts='+checkEmail.ownerid} target="_blank">
                                      <img src={checkEmail.img} style={{marginTop: '10px', width: '100px', borderRadius: '10px', boxShadow: '2px 2px 10px rgba(0,0,0,0.5)'}} />
                                    </a>
                                  </div>
                                }
                              </div>
                              <div style={{marginTop: '5px', textAlign: 'center'}}>
                                <button className="button button-blue">Save</button>
                              </div>
                            </form>
                          </div>
                        </span>
                      }
                      {history.booking_id && history.booking_id != 0 &&
                        <div className="booking_box">
                          <span>Booking id:
                            <a href={"https://www.interbooker.com/admin/booking.php?bkid="+history.booking_id} target="blank"> {history.booking_id}</a>
                          </span>
                        </div>
                      }
                      {history.counterparty != 'anonymous' && this.props.currentWindow == 'history' && voipqNumbers.indexOf(history.counterparty) == -1 &&
                        <button onClick={() => this.calling(history.identity, history.counterparty)} className="call-btn"><i className="fas fa-phone"></i></button>
                      }
                    </div>
                  </div>
                  <div className="counterparty-direction">
                    {history.outbound == 1 ?
                      <i className="fas fa-angle-double-left"></i>
                    :
                      <i className="fas fa-angle-double-right"></i>
                    }
                  </div>
                </div>
                <div className="history-to counterparty-number">

                  <Contact voipqNumbers={voipqNumbers} contact={history.identityUser} />

                  <br />
                  {_.map(history.called_agent, (called_agent, j) => {
                    return(
                      <div className="called-agent" key={j}>
                        <div className="called-agent-name">
                          <div>
                            {called_agent.is_answered == 1 ?
                              called_agent.outbound == 1 ?
                                <img className="answered-img" src="./assets/icons/out.png"/>
                              :
                                <img className="answered-img" src="./assets/icons/inc.png"/>
                            : called_agent.is_answered == 0 ?
                              called_agent.outbound == 1 ?
                                <img className="answered-img" src="./assets/icons/out-mis.png"/>
                              :
                                <img className="answered-img" src="./assets/icons/mis.png"/>
                            :
                              <img className="answered-img" src="./assets/icons/blocked.png"/>
                            }
                            <div className="called-agent-info">
                              <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+called_agent.agentId} target="blank">
                                {called_agent.name}
                              </a>
                              <div>{called_agent.identity_agent}</div>
                            </div>
                          </div>
                          {called_agent.is_answered == 3 && history.transfered &&
                            history.transfered.map(transfered_to_number => {
                              return(
                                <div key={transfered_to_number.tnasfered_id}>
                                  {transfered_to_number.is_answered == 1 ?
                                    <img className="transfer-img" src="./assets/icons/transfer.png"/>
                                  :
                                    <img className="transfer-img" src="./assets/icons/transferMis.png"/>
                                  }
                                  <div style={{display: 'inline-block', width: '80%'}}>
                                    {transfered_to_number.tnasfered_id != 0 &&
                                      <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+transfered_to_number.tnasfered_id} target="blank">
                                        {transfered_to_number.tnasfered_name}
                                      </a>
                                    }
                                    <div>{transfered_to_number.tnasfered_identity}</div>
                                  </div>
                                  <br />
                                </div>
                              );
                            })
                          }
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            );
          })}
          {!historyResult.end &&
            <div style={{textAlign: 'center'}}>
              <img src="/assets/icons/ajax-loader.gif" style={{width: '40px'}} />
            </div>
          }
        </div>
      );
    } else {
      return(<img src="assets/img/loader.gif" style={{display: 'block', marginLeft: 'auto', marginRight: 'auto'}}/>);
    }
  }
}

function mapStateToProps(state){
    return{
      agent: state.agentReducer.agent,
      historyResult: state.historyReducer,
      currentWindow: state.windowReducer.window,
      voipqNumbers: state.voipQReducer,
      checkEmail: state.checkEmailReducer
    }
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getContactInfo,
    setCalledNumbers,
    onCheckEmail,
    saveContactHistory,
    createContactCall,
    counterpartyInfo
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(HistoryBlock);
