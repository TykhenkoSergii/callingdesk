import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {changeWindow, onCalledAgents, getHistory, loadHistory, onSaveBooking} from '../actions';
import axios from 'axios';
import ConferenceParticipant from './components/conferenceParticipant.jsx';
import DialPad from './components/dialPad.jsx';
import Contact from './components/contact.jsx';

class Conference extends Component{

  constructor(props){
    super(props);
    this.state={
      counterparty: null,
      offset: 0,
      historyHeight: 0,
      transferStyle:{
        height: '0'
      },
      timer: {
        hours: 0,
        minutes: 0,
        seconds: 0
      },
      booking_id: '',
      isDialPadOpened: false,
      isHangupOpened: false
    }
  }

  onHangUp(){
    chrome.extension.sendMessage({key: 'hangUp'});
  }

  onHangUpConference(){
    chrome.extension.sendMessage({key: 'hangUpConference'});
  }

  openHangupBox(){
    this.setState({
      isHangupOpened: !this.state.isHangupOpened
    })
  }

  onAccept(){
    chrome.extension.sendMessage({key: 'acceptCall'});

    this.timer(true);
  }

  onHold(){
    chrome.extension.sendMessage({key: 'holdCall'});
  }

  onResume(){
    chrome.extension.sendMessage({key: 'resumeCall'});
  }

  componentDidUpdate(){
    const counterparty = this.props.calledNumber.from != this.props.agent.phonenumber ? this.props.calledNumber.from : this.props.calledNumber.to;

    if(counterparty && this.props.historyResult.counterparty != counterparty &&
      (this.props.currentWindow == 'callingBoxIn' || this.props.currentWindow == 'onHoldIn' || this.props.currentWindow == 'callingBoxOut' || this.props.currentWindow == 'onHoldOut')){
      if(this.state.counterparty != counterparty){
        this.setState({counterparty, offset: 0});
      }
      this.props.getHistory(0, counterparty);

    }

    let historyHeight = window.innerHeight - this.callingMain.offsetTop - this.callingMain.clientHeight - 5;
    if(this.callingTenant)
      historyHeight = historyHeight - this.callingTenant.clientHeight - 24;

    if(historyHeight != this.state.historyHeight)
      this.setState({historyHeight});

  }

  timer(start = false){
    const currentState = this;

    clearInterval(this.timerInterval);

    if(start){
      currentState.setState({
        timer: {
          hours: 0,
          minutes: 0,
          seconds: 0
        }
      });
    }
    this.timerInterval = setInterval(function(){
      if(currentState.state.timer.seconds < 59){
        currentState.setState({
          timer:{
            hours: currentState.state.timer.hours,
            minutes: currentState.state.timer.minutes,
            seconds: currentState.state.timer.seconds + 1,
          }
        });
      } else {
        if(currentState.state.timer.minutes < 59){
          currentState.setState({
            timer:{
              hours: currentState.state.timer.hours,
              minutes: currentState.state.timer.minutes + 1,
              seconds: 0,
            }
          });
        } else {
          currentState.setState({
            timer:{
              hours: currentState.state.timer.hours + 1,
              minutes: 0,
              seconds: 0,
            }
          });
        }
      }
    }, 1000);
  }

  stopTimer(){
    clearInterval(this.timerInterval);
  }

  componentDidMount(){
    const currentState = this;
    chrome.runtime.onMessage.addListener(function(request, sender, callback) {
      switch(request.key) {
        case 'runTimer':
          currentState.setState({
            timer: request.timer
          });
          if(request.timer.seconds > 0 || request.timer.minutes > 0 || request.timer.hours > 0 ){
            currentState.timer();
          } else {
            currentState.stopTimer();
          }
          break;
        case 'startTimer':
          currentState.timer(true);
          break;
        default:
          return null;
      }
    });
  }

  openKeypad(){
    const currentKeypad = this.state.isDialPadOpened;
    this.setState({
      isDialPadOpened: !currentKeypad
    });
  }

  getNumberInfo(number){
    axios.get('https://www.interbooker.com/admin/voip3/accountInfo.php', {withCredentials: true, params: {number, extension: chrome.extension.getURL('/')}}).then(response => {
      chrome.extension.sendMessage({key: 'changeConferenceParticipant', participant: response.data});
    });
  }

  render(){

    const { currentWindow, currentStatus, agent, calledNumber, counterparty, voipqNumbers, conferenceParticipants, changeWindow } = this.props;
    const { isHangupOpened } = this.state;
    console.log(agent)
    return(
      <div className="calling-box">
        <div className="calling-buttons">
          <button onClick={() => this.onAccept()} className="call-btn"><i className="fas fa-phone"></i></button>

          {currentStatus == 'onConferenceHold' ?
            <button onClick={() => this.onResume()} className="resume-btn">Resume</button>
          :
            <button onClick={() => this.onHold()} className="hold-btn">Hold</button>
          }

          <button onClick={() => this.openHangupBox()} className="hangup-btn"><i className="fas fa-phone"></i></button>
          <div className="hangup-ask" style={{display: isHangupOpened ? 'block' : 'none'}}>
            <div className="hangup-title">Do you wish to hangup all conference?</div>
            <div className="hangup-buttons">
              <button onClick={() => this.onHangUpConference()} className="hangup-btn">Yes</button>
              <button onClick={() => this.onHangUp()} className="hangup-btn">No</button>
              <button onClick={() => this.openHangupBox()} className="hangup-btn">Cancel</button>
            </div>
          </div>
          <div className="keypad-btn">
            <img onClick={() => this.openKeypad()} src="./assets/icons/keypad.png" />
            {this.state.isDialPadOpened &&
              <DialPad />
            }
          </div>
        </div>
        <div className="calling-main" ref={(callingMain) => {this.callingMain = callingMain}}>
          <div className="single-history single-history-header">
            <div>Time</div>
            <div>Counterparty</div>
            <div>Identity</div>
          </div>
          <div className="calling-from">
            {this.state.timer.hours < 10 ? '0' + this.state.timer.hours : this.state.timer.hours}:
            {this.state.timer.minutes < 10 ? '0' + this.state.timer.minutes : this.state.timer.minutes}:
            {this.state.timer.seconds < 10 ? '0' + this.state.timer.seconds : this.state.timer.seconds}
          </div>
          <div className="calling-via">
            <div className="counterparty-number">
              <Contact voipqNumbers={voipqNumbers} contact={counterparty.flag ? counterparty : calledNumber.counterparty} />
            </div>
          </div>
          <div className="calling-to history-to">
            <span>
              Conference
            </span>
            <br/>

            <div className="called-agent">
              <img className="answered-img" src="./assets/icons/inc.png" /><div className="history-identity">
                <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+agent.contactid} target="blank">
                  {agent.displayname}
                </a>
                <div>{agent.publicid}</div>
              </div>
            </div>

            {this.props.calledNumber.from != this.props.agent.phonenumber ?
              <div className="called-agent">
                {_.map(this.props.calledAgents, (calledAgent, i) => {
                  if(calledAgent.identity_agent != this.props.agent.phonenumber){
                    return(
                      <div key={i}>
                        <img className="answered-img" src="./assets/icons/inc.png" />
                        <div className="history-identity" key={i}>
                          <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+calledAgent.agent_id} target="blank">
                            {calledAgent.agent_name}
                          </a>
                          <div>{calledAgent.identity_agent}</div>
                        </div>
                      </div>
                    );
                  }
                })}
              </div>
            :
              <br />
            }
          </div>
        </div>
        <div style={{textAlign: "center"}}>
          Conference Participants
          <span className="add-to-conference" onClick={() => changeWindow('calling', 'conference')}> + </span>
        </div>
        {_.map(conferenceParticipants, conferenceParticipant => {

          if(!conferenceParticipant.firstName) this.getNumberInfo(conferenceParticipant.number);

          return(
            <ConferenceParticipant conferenceParticipant = {conferenceParticipant} />
          );

        })}
      </div>
    );
  }

}

function mapStateToProps(state){
    return{
      currentWindow: state.windowReducer.window,
      currentStatus: state.windowReducer.status,
      agent: state.agentReducer.agent,
      calledNumber: state.calledNumberReducer,
      calledAgents: state.calledAgentsReducer,
      historyResult: state.historyReducer,
      voipqNumbers: state.voipQReducer,
      conferenceParticipants: state.conferenceParticipants,
      counterparty: state.counterpartyReducer
    }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    changeWindow,
    onCalledAgents,
    getHistory,
    loadHistory,
    onSaveBooking,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Conference);
