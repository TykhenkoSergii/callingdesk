import React, { Component } from 'react';
import {changeWindow, changeSoundStatus} from '../actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';


class Header extends Component{

  changeWindow(e){
    chrome.extension.sendMessage({key: 'changeWindow', currentWindow: e.target.value});
  }

  onChangeSoundStatus(soundStatus){
    this.props.changeSoundStatus(soundStatus);
    chrome.extension.sendMessage({key: 'changeSoundStatus', soundStatus});
  }

  render(){
    const {soundStatus, currentStatus, contactInfo, agent, currentWindow} = this.props;

    return(
      <header>
        {agent.contact_id &&
          <div>
            <div className="account-info">
              <div className="currentAgent">
                <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+ agent.contact_id} target='_blank'>
                  <img src={agent.img} />
                  {agent.displayname ?
                    <div>{agent.displayname}({agent.phonenumber})</div>
                  :
                    <div> Please Login</div>
                  }
                </a>
              </div>
              <div className="current-counterparty">
              <div style={{height: '50px', width: '50px', overflow: 'hidden', marginTop: '0'}}>
                {soundStatus ?
                  <img src="./assets/icons/mute-sprite.png" onClick={() => this.onChangeSoundStatus(false)} style={{cursor: 'pointer', height: 'auto', marginTop: '-50px'}} />
                :
                  <img src="./assets/icons/mute-sprite.png"  onClick={() => this.onChangeSoundStatus(true)} style={{cursor: 'pointer', height: 'auto'}}/>
                }
              </div>
              {contactInfo.id && (contactInfo.name && contactInfo.name.indexOf('anonymous') == -1 || contactInfo.firstName && contactInfo.firstName.indexOf('anonymous') == -1) ?
                <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+contactInfo.id} target='_blank'>
                  <img src={contactInfo.img} />
                  <div>
                    {contactInfo.firstName ? contactInfo.firstName + ' ': contactInfo.name}
                    {contactInfo.middleName ? contactInfo.middleName + ' ': ''}
                    {contactInfo.lastName ? contactInfo.lastName + ' ': ''}
                  </div>
                </a>
              :
                <a href="https://www.interbooker.com/admin/contact.php?$IdContacts=0" target='_blank'>
                  <img src="https://www.interbooker.com/admin/css/no-user.png" />
                  <div>Create New Contact</div>
                </a>
              }
              </div>
            </div>
            {currentStatus == 'callingOut' || currentStatus == 'onHoldOut' || currentStatus == 'conference' || currentStatus == 'onConferenceHold' ?
              <div className="main-tabs four-buttons">
                <button value="callingBoxOut"
                  className={currentWindow != "calling" && currentWindow != "history" ? 'active-btn' : ''}
                  onClick={(e) => this.changeWindow(e)}
                  style={{color: 'green'}}>
                  Curren Call
                </button>
                <button value="calling" className={currentWindow == "calling" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>New Call</button>
                <button value="history" className={currentWindow == "history" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>History</button>
                <button value="callback" className={currentWindow == "callback" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>Call Back</button>
              </div>
            : currentStatus == 'callingIn' || currentStatus == 'onHoldIn' ?
              <div className="main-tabs four-buttons">
                <button value="callingBoxIn"
                  className={currentWindow != "calling" && currentWindow != "history" ? 'active-btn' : ''}
                  onClick={(e) => this.changeWindow(e)}
                  style={{color: 'green'}}>
                  Curren Call
                </button>
                <button value="calling" className={currentWindow == "calling" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>New Call</button>
                <button value="history" className={currentWindow == "history" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>History</button>
                <button value="callback" className={currentWindow == "callback" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>Call Back</button>
              </div>
            :
              <div className="main-tabs three-buttons">
                <button value="calling" className={!currentWindow || currentWindow == "calling" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>New Call</button>
                <button value="history" className={currentWindow == "history" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>History</button>
                <button value="callback" className={currentWindow == "callback" ? 'active-btn' : ''} onClick={(e) => this.changeWindow(e)}>Call Back</button>
              </div>
            }
          </div>
        }
      </header>
    );

  }
}

function mapStateToProps(state){
    return{
      currentWindow: state.windowReducer.window,
      currentStatus: state.windowReducer.status,
      agent: state.agentReducer.agent,
      contactInfo: state.contactInfoReducer,
      soundStatus: state.soundStatusReducer
    }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    changeWindow,
    changeSoundStatus
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
