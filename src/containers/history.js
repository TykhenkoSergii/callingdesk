import React, { Component } from 'react';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import _ from 'lodash';
import {getHistory, loadHistory} from '../actions';
import HistoryBlock from './components/historyBlock';

class History extends Component{

  constructor(props){
    super(props);
    this.state = {
      offset: 0
    }
  }

  onHistoryScroll(e){

    const { agent } = this.props;

    if(e.target.scrollTop > this.historyWraper.clientHeight - this.historyDiv.clientHeight - 200 && this.state.offset == this.props.historyResult.offset ){
      const newOffset = this.state.offset + 20;
      this.props.getHistory(newOffset);
      this.setState({
        offset: newOffset
      });
    }

  }

  componentDidMount(){

    const { agent } = this.props;
    console.log('FFFFFFFFFFFF')
    console.log(agent)
    this.props.getHistory(0);
  }

  componentDidUpdate(){

    const { agent } = this.props;
    if(this.props.historyResult.counterparty && this.props.currentWindow == 'history'){
      this.props.loadHistory({});
      this.props.getHistory(0);

      this.setState({
        offset: 0
      })
    }
  }

  render(){
    return(
      <div>
        <div className="single-history single-history-header">
          <div>Time</div>
          <div>Counterparty</div>
          <div>Identity</div>
        </div>
        <div className='history' onScroll={(e) => this.onHistoryScroll(e)} ref={(historyDiv) => {this.historyDiv = historyDiv}}>
          <div className="history-wraper" ref={(historyWraper) => {this.historyWraper = historyWraper}}>
            <HistoryBlock trigeredEvent={this.props.trigeredEvent}/>
          </div>
        </div>
      </div>);
  }
}

function mapStateToProps(state){
    return{
      currentWindow: state.windowReducer.window,
      historyResult: state.historyReducer,
      agent: state.agentReducer.agent
    }
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getHistory,
    loadHistory
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(History);
