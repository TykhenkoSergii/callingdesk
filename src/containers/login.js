import React, {Component} from 'react';
import {connect} from 'react-redux';


class Login extends Component{

  render(){
    //If microphone permission denied
    if(this.props.microphoneError){
      return(
        <div className="login-page">
          <h1>{this.props.microphoneError}</h1>
        </div>
      );
    //If not Loged in
    } else {
      return(
        <div className="login-page">
          <h1>Please login at <a href="https://www.interbooker.com/admin" target="_blank">www.interbooker.com</a></h1>
        </div>
      );
    }
  }

}

function mapStateToProps(state){
    return{
      microphoneError: state.microphoneErrorReducer.error
    }
}

export default connect(mapStateToProps)(Login);
