import React, { Component } from 'react';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {changeWindow, setCalledNumbers, onCalledAgents, loadAgent, setMicrophoneError, counterpartyInfo,
      callingInnerNumber, changeSoundStatus, saveOnlineAgents, loadConferenceParticipants, clearCalledNumbers} from '../actions';
import Header from './header';
import History from './history';
import NewCall from './newCall';
import CallingBox from './callingBox';
import Conference from './conference';
import Login from './login';
import CallBack from './callBack';

class Main extends Component{

  constructor(props){
    super(props);
    this.state={
      trigeredEvent: null
    }
  }

  componentDidMount(){
    const currentState = this;
    let  microphoneError;

    //Checking microphone permission
    navigator.webkitGetUserMedia(
      {
        audio: true
      },
      function(stream) {
        //checking what window should be opened at first and if there is some call and is agent already saved on background
        chrome.extension.sendMessage({key: 'checkWindow'}, function(checkRes){

          if(checkRes.status == 'callingIn' || checkRes.status == 'callingOut' || checkRes.status == 'conference'){

            currentState.props.setCalledNumbers(checkRes.callSession.counterparty, checkRes.callSession.identity, checkRes.callSession.direction);
            currentState.props.changeWindow(checkRes.currentWindow, checkRes.status);

            if(checkRes.status != 'conference'){
              currentState.props.counterpartyInfo(checkRes.callSession.counterparty);
            } else {
              currentState.props.loadConferenceParticipants(checkRes.conferenceParticipants);
            }
            //loading old agent date
            chrome.extension.sendMessage({key: 'checkAgent'}, function(res){

              currentState.props.loadAgent(res.agent);

            });
          } else {
            //Checking agent data
            axios.get('https://www.interbooker.com/admin/voip3/agentGuid.php', {withCredentials: true, params: {extension: chrome.extension.getURL('/')}}).then(response => {

              if(!response.data){
                 currentState.props.changeWindow('login');
                 chrome.extension.sendMessage({key: 'deleteAgent'});
                 chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
              } else {
                chrome.extension.sendMessage({key: 'checkAgent', guid: response.data}, function(res){
                  if(res.isNew){
                    //Loading new agent data
                    axios.get('https://www.interbooker.com/admin/voip3/getAgentData.php', {withCredentials: true, params: {extension: chrome.extension.getURL('/')}}).then(response => {
                      if(response.data.publicid && response.data.password && response.data.server){
                        chrome.extension.sendMessage({key: 'newAgent', agent: response.data});
                        currentState.props.loadAgent(response.data);
                      } else {
                        let error = <div>{"Telephony account is missing. Please check and set it up correctly."}</div>;
                        currentState.props.changeWindow('login');
                        currentState.props.setMicrophoneError(error);
                        chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
                      }
                    });

                  } else {
                    //Loading Agent's available numbers
                    currentState.props.loadAgent(res.agent);
                  }

                });
              }
            });
          }
          currentState.props.changeSoundStatus(checkRes.soundStatus);
        });
      },
      function(error) {
      	if(error.name=="DevicesNotFoundError"){
      		microphoneError= <div>{"Can't found device. Check your microphone"}<br/>{"and click "}<a href={chrome.extension.getURL('background.html')} target='_blank'>here</a>{" to try again"}</div>;
      	}else if(error.name=="PermissionDeniedError"){
      		microphoneError= <div>{"You have blocked permission for microphone"}<br/>{"Click "}<a href={chrome.extension.getURL('background.html')} target='_blank'>here</a>{" to fix it"}</div>;
      	}else{
      		microphoneError= <div>{"Need permission for use microphone"}<br/>{"Click "}<a href={chrome.extension.getURL('background.html')} target='_blank'>here</a>{" to fix it"}</div>;
      	};

        currentState.props.changeWindow('login');
        currentState.props.setMicrophoneError(microphoneError);
        chrome.browserAction.setIcon({'path': "assets/icons/icon3.png"});
        return false;
      }
    );


    chrome.extension.onMessage.addListener(function(request, sender, callback) {
      if(currentState.props.currentWindow != 'login'){
        switch(request.key) {
          case 'onChangeWindow':
            if(request.status == 'callingIn'){
              currentState.props.onCalledAgents(currentState.props.agent.phonenumber);
            }
            currentState.props.changeWindow(request.currentWindow, request.status);
            break;
          case 'onCallingIn':
            currentState.props.setCalledNumbers(request.callSession.counterparty, request.callSession.identity, request.callSession.direction);
            currentState.props.counterpartyInfo(request.callSession.counterparty);
            if(request.callSession.fromInternalName){
              currentState.props.callingInnerNumber(request.callSession.counterparty, request.callSession.fromInternalName);
            }
            currentState.props.changeWindow(request.currentWindow, request.status);
            setTimeout(() => currentState.props.onCalledAgents(currentState.props.agent.phonenumber), 1000);
            break;

          case 'onlineAgents':

            currentState.props.saveOnlineAgents(request.online_agents);
            break;

          case 'onCallingConference':
            currentState.props.changeWindow(request.currentWindow, request.status);
            currentState.props.setCalledNumbers(request.callSession.counterparty, request.callSession.identity, request.callSession.direction);
            currentState.props.loadConferenceParticipants(request.conferenceParticipants);
            break;

          case 'onAddToConference':
            currentState.props.changeWindow(request.currentWindow, request.status);
            currentState.props.loadConferenceParticipants(request.conferenceParticipants);
            break;

          case 'onConferenceParticipantChange':
              currentState.props.loadConferenceParticipants(request.conferenceParticipants);
              break;

          case 'onClearCall':
              currentState.props.loadConferenceParticipants(request.conferenceParticipants);
              break;

          default:
            return null;
        }
      }
    });
  }

  closePopups(e){
    this.setState({trigeredEvent: {...e}});
  }

  render(){
    const { currentStatus, currentWindow, agent } = this.props;

    //If Loged in
    if(currentWindow != 'login' && agent.guid){
      return(
        <div>
          <Header />
          <div className={!currentWindow || currentWindow == 'calling' ? 'calling' : 'calling disabled'}>
            <NewCall  />
          </div>
          <div className={currentWindow == 'callingBoxIn' || currentWindow == 'onHoldIn' ? 'callingBoxIn' :
            currentWindow == 'callingBoxOut' || currentWindow == 'onHoldOut' ? 'callingBoxOut' : 'callingBox disabled'}>
            {currentStatus == 'conference' || currentStatus == 'onConferenceHold' ?
              <Conference  />
            :
              <CallingBox  />
            }
          </div>
          <div className={currentWindow == 'history' ? '' : 'history disabled'} onClick={(e) => this.closePopups(e)}>
            <History trigeredEvent={this.state.trigeredEvent}/>
          </div>
          <div className={currentWindow == 'callback' ? 'callback' : 'callback disabled'}>
            <CallBack trigeredEvent={this.state.trigeredEvent}/>
          </div>

        </div>
      );

    //If not Loged in
    } else if(currentWindow == 'login') {
      return <Login  />;
    } else {
      return <img src="assets/img/loader.gif" style={{display: 'block', marginTop: '180px', marginLeft: 'auto', marginRight: 'auto'}}/>;
    }
  }
}

function mapStateToProps(state){
    return{
      currentWindow: state.windowReducer.window,
      currentStatus: state.windowReducer.status,
      agent: state.agentReducer.agent
    }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    loadAgent,
    changeWindow,
    setCalledNumbers,
    clearCalledNumbers,
    callingInnerNumber,
    onCalledAgents,
    setMicrophoneError,
    changeSoundStatus,
    saveOnlineAgents,
    loadConferenceParticipants,
    counterpartyInfo
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
