import React, { Component } from 'react';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import _ from 'lodash';
import {loadAgent, changeWindow, createSession, setCalledNumbers, getContactInfo, counterpartyInfo,
  setMicrophoneError, onCalledAgents, onAllContacts, onChoosingCounterparty, onCheckEmail, saveContactCall, createContactCall, searchByContact} from '../actions';

class NewCall extends Component{

  constructor(props){
    super(props);
    this.state = {
      agent: 'Not loged in',
      filterCounterparty: '',
      filterNumber: '',
      checkEmail: '',
      choosedNumber: null,
      choosedCounterparty: null,
      ownerid: null,
      sipStack: null,
      filtered: false,
      justChoosedCounterparty: false,
      justChoosedIdentity: false,
      searchTimeout: false,
      emailTimeout: false,
      counterparty: null,
      emailCheckbox: false,
      contactName: '',
      emailError: false,
      searchingAccounts: false,
      startCheckingEmail: false,
      initiateCall: false,
      justSearchedCounterparty: false,
    }
    this.agentNumber = {};
    this.agentBox = null;

  }

  componentDidMount(){
    const currentState = this;
    chrome.extension.onMessage.addListener(function(request, sender, callback) {
      if(currentState.props.currentWindow != 'login'){
        switch(request.key) {

          case 'onSiteCallBtn':

            currentState.props.searchByContact(request.counterparty.counterparty_id);
            currentState.props.changeWindow('calling');

            if(request.identity){

              const {publicid, ownerid} = request.identity;

              currentState.setState({
                filterCounterparty: request.counterparty.counterparty_name,
                choosedCounterparty: {number: request.counterparty.publicid},
                counterparty: {number: request.counterparty.publicid},
                number: publicid,
                ownerid,
                initiateCall: true
              });
            } else {
              currentState.setState({
                filterCounterparty: request.counterparty.counterparty_name
              });
            }
            callback('success');
            break;

        }
      }
    });
  }

  componentWillUpdate(nextProps){

  }

  componentDidUpdate(){
    const {allContacts, getContactInfo, contactInfo, agent} = this.props;

    let sameNumber = false;
    if(this.state.justSearchedCounterparty){
      _.forEach(allContacts, (counterparty, i) => {
        {_.forEach(counterparty.accounts, (account, j) => {
          if(account.number == this.state.filterCounterparty){
            this.setState({
              choosedCounterparty: i + '.' + j,
              counterparty: {...account, number: account.number},
              justSearchedCounterparty: false
            });
            getContactInfo(account);
          }
        })}
      });
    }

    if(allContacts && Object.keys(allContacts).length == 1 && allContacts[Object.keys(allContacts)[0]].accounts.length == 1 &&
    (!this.state.counterparty || !this.state.counterparty.contact_id || this.state.counterparty.contact_id !== allContacts[Object.keys(allContacts)[0]].contact_id)
    && this.state.filterCounterparty.length > 9){
      this.setState({
        choosedCounterparty: Object.keys(allContacts)[0] + '.0',
        counterparty: {...allContacts[Object.keys(allContacts)[0]], number: allContacts[Object.keys(allContacts)[0]].accounts[0].number},
        justChoosedCounterparty: true
      });

      getContactInfo({...allContacts[Object.keys(allContacts)[0]], number: allContacts[Object.keys(allContacts)[0]].accounts[0].number});

    } else if(contactInfo.identity != null && agent.numbers_object && ((agent.numbers_object[contactInfo.identity] && this.state.counterparty && contactInfo.guid == this.state.counterparty.guid) || contactInfo.identity == 'inner') && this.state.justChoosedCounterparty  ){
      //showing from wich number was last call to choosed counterparty

      if(contactInfo.identity != 'inner'){
        let identityNumber = agent.numbers_object[contactInfo.identity];
        // const currentEl = this.agentNumber[contactInfo.identity];
        // currentEl.scrollIntoView({ behavior: "smooth" });
        this.setState({
          number: identityNumber.phonenumber,
          choosedNumber: -1,
          ownerid: identityNumber.ownerid,
          justChoosedCounterparty: 0,
          justChoosedIdentity: 0,
          filterNumber: identityNumber.phonenumber,
          numberInput: true
        });
     } else if(this.state.filterCounterparty.length > 0 && contactInfo.identity == 'inner') {
        this.agentBox.scrollIntoView({ behavior: "smooth" });
        this.setState({
          number: agent.phonenumber,
          ownerid: agent.contactid,
          filterNumber: agent.phonenumber,
          choosedNumber: agent.phonenumber,
          justChoosedCounterparty: 0,
          justChoosedIdentity: 0,
        });
     }
   }
    if(this.state.counterpartyInput){
      this.counterpartyInput.select();
      this.setState({
        counterpartyInput: false
      });
    }
    if(this.state.numberInput){
      this.numberInput.select();
      this.setState({
        numberInput: false
      });
    }
    if(this.state.initiateCall && this.props.agent.phonenumber){
      this.setState({
        initiateCall: false
      });
      this.calling();
   }
  }

  chooseNumber(number, choosed, ownerid){

    this.setState({
      number: number,
      choosedNumber: choosed,
      ownerid: ownerid,
      justChoosedIdentity: false,
      filterNumber: number,
      numberInput: true
    });
  }

  onNumberChange(e){

    let filterNumber;
    if(e.target.value.match(/\d/) && !e.target.value[0].match(/[A-Za-z]/)){
      filterNumber = e.target.value.replace(/\D+/g, '');
    } else {
      filterNumber = e.target.value;
    }

    this.setState({
      filterNumber,
      choosedNumber: null
    });
  }

  chooseCounterparty(counterparty, choosed, number){
    if(counterparty.guid){
      this.props.getContactInfo(counterparty);
      counterparty.number = number;
    } else {
      this.props.getContactInfo(counterparty.number);
    }
    this.setState({
      counterparty: counterparty,
      choosedCounterparty: choosed,
      filterCounterparty: counterparty.number,
      justChoosedCounterparty: true,
      counterpartyInput: true,
      checkEmail: '',
      justSearchedCounterparty: false,
    });
    this.props.onChoosingCounterparty();
  }

  onCounterpartyChange(e){
    const currentState = this;
    const target = e.target;
    let counterpartyNumber;
    if(target.value.match(/\d/) && !target.value[0].match(/[A-Za-z]/)){
      counterpartyNumber = target.value.replace(/\D+/g, '');
    } else {
      counterpartyNumber = target.value;
    }

    let searchTimeout = this.state.searchTimeout;
    clearTimeout(searchTimeout);

    searchTimeout = setTimeout(function(){
        currentState.props.onAllContacts(target.value);
        currentState.props.getContactInfo(counterpartyNumber);
        currentState.setState({
          searchingAccounts: true,
          justSearchedCounterparty: true,
        });
    }, 500);

    if(counterpartyNumber){
      this.setState({
        counterparty: {number: counterpartyNumber},
        filterCounterparty: counterpartyNumber,
        choosedCounterparty: null,
        filtered: true,
        searchTimeout,
        checkEmail: ''
      });
    } else {
      this.setState({
        counterparty: {number: counterpartyNumber},
        filterCounterparty: counterpartyNumber.replace('+', ''),
        choosedCounterparty: null,
        filtered: false,
        searchTimeout,
        checkEmail: ''
      });
    }
  }

  calling(){
    const { counterparty } = this.state;
    const { agent } = this.props;

    if(this.state.checkEmail.length > 0 && this.props.checkEmail.publicid == this.state.checkEmail && this.props.checkEmail.displayname){
      if(!isNaN(+this.state.checkEmail)){
        this.props.saveContactCall(this.props.checkEmail, this.state.filterCounterparty, this.state.checkEmail);
      } else {
        this.props.saveContactCall(this.props.checkEmail, this.state.filterCounterparty);
      }
    } else if(this.props.checkEmail.publicid == this.state.checkEmail && !this.props.checkEmail.displayname && this.state.contactName.length > 0) {
      this.props.createContactCall(this.state.filterCounterparty, this.state.contactName, this.state.checkEmail);
    } else if(this.state.emailCheckbox && !this.props.checkEmail.displayname && this.state.contactName.length > 0) {
      this.props.createContactCall(this.state.filterCounterparty, this.state.contactName, '');
    } else {
      this.props.counterpartyInfo(counterparty.number);
    }

    if(this.state.counterparty.number.indexOf('0') === 0 && this.state.counterparty.number.indexOf('00') !== 0){
      counterparty.number = this.state.number.substr(0, 2) + this.state.counterparty.number;
    } else if(this.state.counterparty.number.indexOf('00') === 0){
      counterparty.number = this.state.counterparty.number.substr(2);
    } else if(this.state.counterparty.number.indexOf('+') === 0){
      counterparty.number = this.state.counterparty.number.substr(1);
    }

    this.props.setCalledNumbers(counterparty.number, this.state.number, 'out');

    const identity = this.props.agent.accounts[this.state.ownerid] ?
      {
        name: agent.accounts[this.state.ownerid].name,
        ownerid: agent.accounts[this.state.ownerid].ownerid,
        account_img: agent.accounts[this.state.ownerid].account_img,
        phonenumber: agent.accounts[this.state.ownerid].numbers_obj[this.state.number].phonenumber,
        displayname: agent.accounts[this.state.ownerid].numbers_obj[this.state.number].displayname,
        flag: agent.accounts[this.state.ownerid].numbers_obj[this.state.number].flag
      }
    : 'inner';

    chrome.extension.sendMessage({key: 'initCall', identity, counterparty});

  }

  addToConference(){
    const {counterparty, number} = this.state;

    if(counterparty.number.indexOf('0') === 0 && counterparty.number.indexOf('00') !== 0){
      counterparty.number = number.substr(0, 2) + counterparty.number;
    } else if(counterparty.number.indexOf('00') === 0){
      counterparty.number = counterparty.number.substr(2);
    } else if(counterparty.number.indexOf('+') === 0){
      counterparty.number = counterparty.number.substr(1);
    }

    chrome.extension.sendMessage({key: 'addToConference', number, counterparty});
  }

  clearInput(inputField){
    this.setState({
      [inputField]: ''
    })
  }

  checkEmail(input){
    if(input.length < 3 || (input.indexOf('@') < 1 || input.indexOf('.') < 1 ||
      (input.indexOf('.') > 0 && input.lastIndexOf('.') < input.indexOf('@') + 1) || input.lastIndexOf('.') == input.length - 1) && isNaN(+input)){
      this.setState({
        emailError: true,
        checkEmail: input
      });
    } else if(input.length > 0) {
      this.setState({
        emailError: false,
        checkEmail: input
      });

      const currentState = this;
      let emailTimeout = this.state.emailTimeout;
      clearTimeout(emailTimeout);

      emailTimeout = setTimeout(function(){
          currentState.props.onCheckEmail(input);
          currentState.setState({
            startCheckingEmail: true
          });
      }, 500);

      this.setState({
        emailTimeout
      });

    } else {
      this.setState({
        emailError: false,
        checkEmail: ''
      });
    }
  }

  emailCheckboxChange(){
    this.setState({
      emailCheckbox: !this.state.emailCheckbox
    });
  }

  onChangeName(name){
    this.setState({
      contactName: name
    })
  }

  componentWillReceiveProps(nextProps){

    this.setState({
      searchingAccounts: false,
      startCheckingEmail: false
    });

  }

  render(){
    const {agent, allContacts, contactInfo, onlineAgents, checkEmail, currentStatus} = this.props;
    if(agent.guid){
      return(
        <div className='calling-page'>
          <div className="calling-header">
            {this.state.ownerid != null ?
              <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+this.state.ownerid}  target="_blank" >Open identity</a>
            :
              <a href="" className="blocked">Open identity</a>
            }

            {this.state.choosedNumber != null && currentStatus != 'conference' && (this.state.choosedCounterparty != null || this.state.filterCounterparty.length < 9 ||
              (this.state.checkEmail == checkEmail.publicid && checkEmail.displayname) ||
              ((this.state.emailCheckbox || (this.state.checkEmail && !this.state.emailError && isNaN(+this.state.checkEmail))) && this.state.contactName.length > 3)) ?
              <button onClick={() => this.calling()} className="call-btn"><i className="fas fa-phone"></i></button>
            : this.state.choosedNumber != null && (this.state.choosedCounterparty != null || this.state.filterCounterparty.length < 9 ||
              (this.state.checkEmail == checkEmail.publicid && checkEmail.displayname) ||
              ((this.state.emailCheckbox || (this.state.checkEmail && !this.state.emailError && isNaN(+this.state.checkEmail))) && this.state.contactName.length > 3)) ?
              <button onClick={() => this.addToConference()} className="call-btn"> <span className="plus-in-btn">+</span> </button>
            : currentStatus != 'conference' ?
              <button className="call-btn blocked" disabled><i className="fas fa-phone"></i></button>
            :
              <button className="call-btn blocked" disabled> + </button>
            }
          </div>
          <div className="calling-numbers-wraper">
            <div className="input-div">
              <input type="text" ref={el => this.counterpartyInput = el}
                onChange={(e) => this.onCounterpartyChange(e)}
                placeholder="Type counterparty name, number, email, booking#..."
                value={this.state.filterCounterparty}/>
              <img src="./assets/img/loader-sm.gif" className="loader-sm" style={this.state.searchingAccounts ? {display: 'block'} : {}}/>

              <span className="isInput"
                onClick={() => this.clearInput('filterCounterparty')}
                style={this.state.filterCounterparty.length > 0 ? {right: '0'} : {}}>X</span>

              <span className="inputLabel">
                Counterparty
              </span>
              {allContacts && _.keys(allContacts).length == 0 && this.state.filterCounterparty.length > 0 && !isNaN(+this.state.filterCounterparty) &&
                  <i style={{fontSize: '10px', position: 'absolute', bottom: '1px', left: '10px', color: 'red'}}>{"Unknown phone number"}</i>
              }
            </div>
            {allContacts && this.state.filterCounterparty.length > 0 ?
              <div className="calling-numbers">
                {_.map(allContacts, (counterparty, i) => {
                  return(
                    <div className="calling-number-block" key={i}>
                      <img src={counterparty.img} />
                      <div className="loaded-user">
                        <div>
                          <a href={'https://www.interbooker.com/admin/contact.php?IdContacts=' + counterparty.contact_id} target="_blank">
                            {counterparty.name ? counterparty.name + ' ' : ''}
                            {counterparty.isBooking ? '(booking)' : ''}
                          </a>
                        </div>
                        {_.map(counterparty.accounts, (account, j) => {
                          return(
                            <div key={i+'.'+j} className={this.state.choosedCounterparty == i+'.'+j ? "choosed calling-number" : "calling-number"}
                              onClick={() => this.chooseCounterparty(counterparty, i+'.'+j, account.number)}>
                              {account.flag && account.flag != 'voip' ?
                                <img className="flag" src={"http://www.geonames.org/flags/x/"+account.flag+".gif"} />
                              : account.flag && account.flag == 'voip' ?
                                <img className="flag" src={"assets/img/globe2.png"} />
                              :
                                <span className="anonymous-flag">?</span>
                              }
                              <span>{account.number} ({account.displayname})</span>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  );
                })}
                <div className="create-new-contact">
                  <div className="text-center">Create new contact</div>
                  <div>
                    <div style={{position: "relative"}}>
                      <input onChange={(e) => this.checkEmail(e.target.value)} type="text" value={!this.state.emailCheckbox ? this.state.checkEmail : ''}
                        placeholder={!this.state.emailCheckbox ? 'Type email or booking#...' : ''}
                        style={this.state.emailError ? {borderBottom: '1px solid #ff0000', width: '90%', color: '#ff0000'} : {borderBottom: '1px solid #000', width: '93%'}} disabled={this.state.emailCheckbox} />
                      <img src="./assets/img/loader-sm.gif" className="loader-sm" style={this.state.startCheckingEmail ? {display: 'block'} : {}}/>
                      {!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !isNaN(+this.state.checkEmail) ?
                        <div style={{fontSize: '10px', position: 'absolute', bottom: '1px', left: '10px', color: 'red'}}>Unknown booking#</div>
                      : !checkEmail.displayname && checkEmail.publicid == this.state.checkEmail &&
                        <div style={{fontSize: '10px', position: 'absolute', bottom: '1px', left: '10px', color: 'red'}}>Unknown email</div>
                      }
                    </div>
                    <div style={{paddingTop: '10px'}}>
                      <input id="email_checkbox" type="checkbox" value={this.state.emailCheckbox}
                        onChange={() => this.emailCheckboxChange()} checked={this.state.emailCheckbox} style={{verticalAlign: 'middle'}}/>
                      <lable for="email_checkbox">{"Both email and booking# are unknown"}</lable>
                    </div>
                  </div>
                  <div>
                    {this.state.emailCheckbox || (!checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && this.state.checkEmail.length > 1 && !this.state.emailError) ?
                      <div>
                        <input onChange={(e) => this.onChangeName(e.target.value)} type="text" value={this.state.contactName} placeholder="name" style={{borderBottom: '1px solid #000', width: '93%'}} />
                        <div style={{color: 'red', marginTop: '5px', fontWeight: '600'}}>Name is required</div>
                      </div>
                    : checkEmail.displayname && checkEmail.publicid == this.state.checkEmail && !this.state.emailError &&
                      <div>
                        <input type="text" value={checkEmail.displayname} style={{borderBottom: '1px solid #000', width: '90%'}} disabled={true}/>
                        {isNaN(+this.state.checkEmail) ?
                          <div style={{marginTop: '5px', fontWeight: '600'}}>Email Found</div>
                        :
                          <div style={{marginTop: '5px', fontWeight: '600'}}>Booking Found</div>
                        }
                        <a href={'https://www.interbooker.com/admin/contact.php?IdContacts='+checkEmail.ownerid} target="_blank">
                          <img src={checkEmail.img} style={{marginTop: '10px', width: '100px', borderRadius: '10px', boxShadow: '2px 2px 10px rgba(0,0,0,0.5)'}} />
                        </a>
                      </div>
                    }
                  </div>
                </div>
              </div>
            :
              <div className="calling-numbers">
                {_.map(onlineAgents, onlineAgent => {
                  if(onlineAgent.guid != agent.contactguid){
                    return(
                      <div key={onlineAgent.guid} className={this.state.choosedCounterparty == onlineAgent.guid ? "choosed calling-number-block onlineAgent" : "calling-number-block onlineAgent"} key={onlineAgent.guid}
                        onClick={() => this.chooseCounterparty(onlineAgent, onlineAgent.guid, onlineAgent.number)}>
                        <img src={onlineAgent.img} />
                        <div className="loaded-user">
                          <div>
                            <span className="statusOnline"></span>
                            {onlineAgent.name ? onlineAgent.name + ' ' : ''}
                          </div>
                            <div className="calling-number">
                              <img className="flag" src={"assets/img/globe2.png"} />
                              <span>{onlineAgent.number}</span>
                            </div>
                        </div>
                      </div>
                    );
                  }
                })}
              </div>
            }
          </div>
          <div className="agent-numbers-wraper">
            <div className="input-div">
              <input type="text" ref={el => this.numberInput = el}
                onChange={(e) => this.onNumberChange(e)}
                placeholder="Type identity name or number..."
                value={this.state.filterNumber}/>
              <span className="isInput"
                onClick={() => this.clearInput('filterNumber')}
                style={this.state.filterNumber.length > 0 ? {right: '0'} : {}}>X</span>
              <span className="inputLabel">
                Identity
              </span>
            </div>
            <div className="agent-numbers">
              {_.map(agent.accounts, (account, i) => {
                let filteredAccount = false;
                _.map(account.numbers_obj, number => {
                  if(number.phonenumber &&
                     (number.phonenumber.indexOf(this.state.filterNumber) != -1 ||
                     (number.displayname && number.displayname.toLowerCase().indexOf(this.state.filterNumber.toLowerCase()) != -1) ||
                     (account.name && account.name.toLowerCase().indexOf(this.state.filterNumber.toLowerCase()) != -1)))
                    filteredAccount = true;
                });
                if(!this.state.filterNumber || filteredAccount){
                  return(
                    <div className="agent-box" key={i}>
                      <img src={account.account_img} />
                      <div className="account-info">
                        <div>
                          {account.name ? account.name + ' ': ''}
                        </div>
                         {_.map(account.numbers_obj, (number, j) => {
                          if(number.phonenumber &&
                             (number.phonenumber.indexOf(this.state.filterNumber) != -1 ||
                             number.displayname.toLowerCase().indexOf(this.state.filterNumber.toLowerCase()) != -1 ||
                             (account.name && account.name.toLowerCase().indexOf(this.state.filterNumber.toLowerCase()) != -1))){
                            return(
                              <div key={i+'.'+j}
                                ref={el => {this.agentNumber[number.phonenumber] = el}}
                                className={(this.state.choosedNumber == i+'.'+j) || (this.state.justChoosedIdentity === 0 && number.phonenumber == contactInfo.identity) ? "choosed agent-number" : "agent-number"}
                                onClick={() => this.chooseNumber(number.phonenumber, i+'.'+j, account.ownerid)}>

                                {number.flag && number.flag != 'voip' ?
                                  <img className="flag" src={"http://www.geonames.org/flags/x/"+number.flag+".gif"} />
                                :
                                  <img className="flag" src={"assets/img/globe2.png"} />
                                }

                                <div>{number.phonenumber} ({number.displayname})</div>

                              </div>
                            );
                          }
                        })}
                      </div>
                    </div>
                  );
                }
              })}
              <div className="agent-box"
                ref={el => {this.agentBox = el}}>
                <img src={agent.toHash} />
                <div className="account-info">
                  <div>
                    {agent.displayname ? agent.displayname + ' ': ''}
                  </div>
                  <div
                    className={(this.state.choosedNumber == agent.phonenumber) || (this.state.justChoosedIdentity === 0 && agent.phonenumber == contactInfo.identity) ? "choosed agent-number" : "agent-number"}
                    onClick={() => this.chooseNumber(agent.phonenumber, agent.phonenumber, agent.contactid)}>

                    <img className="flag" src={"assets/img/globe2.png"} />

                    <div>{agent.phonenumber}</div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );

    } else {
      return(<img src="assets/img/loader.gif" style={{display: 'block', marginTop: '180px', marginLeft: 'auto', marginRight: 'auto'}}/>);
    }
  }
}

function mapStateToProps(state){
    return{
      agent: state.agentReducer.agent,
      allContacts: state.allContactsReducer.contacts,
      contactInfo: state.contactInfoReducer,
      onlineAgents: state.onlineAgentsReducer,
      checkEmail: state.checkEmailReducer,
      currentStatus: state.windowReducer.status,
    }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    loadAgent,
    changeWindow,
    createSession: createSession,
    setCalledNumbers,
    getContactInfo,
    setMicrophoneError,
    onCalledAgents,
    onAllContacts,
    onChoosingCounterparty,
    onCheckEmail,
    saveContactCall,
    createContactCall,
    searchByContact,
    counterpartyInfo
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCall);
