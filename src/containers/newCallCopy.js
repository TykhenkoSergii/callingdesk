import React, { Component } from 'react';
import {sipStack} from '../sipml5';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import _ from 'lodash';
import {getNumbers, loadAgent, changeWindow, createSession, setCalledNumbers, getContactInfo} from '../actions';

class NewCall extends Component{

  constructor(props){
    super(props);
    this.state = {
      agent: 'Not loged in',
      agentNumber: null,
      filterCounterparty: '',
      filterNumber: '',
      choosedNumber: null,
      choosedCounterparty: null,
      ownerid: null,
      counterpartyid: null,
      sipStack: null
    }
  }

  componentDidMount(){

    this.props.getNumbers();


    axios.get('https://www.interbooker.com/admin/voip3/agentData.php', {withCredentials: true}).then(response => {

      if(!response.data.EmployeesGUID){
        this.props.changeWindow('login');
        return false;
      }

      this.setState({
        agent: response.data.displayname,
        guid: response.data.EmployeesGUID,
        agentNumber: response.data.phonenumber,
      });

      this.props.loadAgent(response.data);

      const currentState = this;

      let sipStack;

      const createStack = () => {
          sipStack = new SIPml.Stack({
            realm: response.data.server,
            impi: response.data.phonenumber,
            impu: 'sip:' + response.data.phonenumber + '@callingdesk.com',
            password: response.data.password,
            display_name: response.data.phonenumber,
            enable_rtcweb_breaker: true,
            bandwidth: { audio:64 },
            ice_servers: [{url:'stun:stun.ekiga.net'}],
            outbound_proxy_url:'udp://callingdesk.com:5060',
            websocket_proxy_url: 'wss://callingdesk.com:8089/ws',
            events_listener: { events: '*',
              listener: function(e){
                console.info('stack session event = ' + e.type);
                if(e.type == 'failed_to_start'){
                  createStack();
                } else if(e.type == 'started'){
                  login();
                } else if(e.type == 'i_new_call'){ // incoming audio/video call
                  currentState.props.setCalledNumbers(e.o_event.o_session.o_uri_from.s_user_name, e.o_event.o_message.o_hdr_From.s_display_name.replace(e.o_event.o_session.o_uri_from.s_user_name, ''), currentState.state.agentNumber);
                  currentState.props.getContactInfo(e.o_event.o_session.o_uri_from.s_user_name);
                  e.newSession.setConfiguration({
                    events_listener: { events: '*', listener: function(e){
                      console.info('session event = ' + e.type);
                      if(e.type == 'terminated'){
                        currentState.props.changeWindow('calling');
                      }
                    }},
                  })
                  currentState.props.createSession(e.newSession);
                  currentState.props.changeWindow('callingBoxIn');
                }
              }
            }
          });
          sipStack.start();
          SIPml.init(sipStack);

          var registerSession;
          var eventsListener = function(e){
              console.info('session event = ' + e.type);
          }

          var login = function(){
              registerSession = sipStack.newSession('register', {
                  events_listener: { events: '*', listener: eventsListener } // optional: '*' means all events
              });
              registerSession.register();
          }

          currentState.setState({
            sipStack: sipStack
          });
        }
        createStack();
    });
  }

  calling(){
      const currentState = this;
      var callingEventsListener = function(e){
          console.info('session event = ' + e.type);
          if(e.type == 'terminated'){
            currentState.props.changeWindow('calling');
          }
      }
      const callSession = this.state.sipStack.newSession('call-audio', {
          audio_remote: document.getElementById('audio-remote'),
          events_listener: { events: '*', listener: callingEventsListener },

      });

      for(var key in SIPml.Stack.prototype.ao_sessions){
          if(SIPml.Stack.prototype.ao_sessions[key]){
              if(true){
                  SIPml.Stack.prototype.ao_sessions[key].o_session.o_uri_from.s_user_name='99'+this.state.number;
                  SIPml.Stack.prototype.ao_sessions[key].o_session.o_uri_from.s_display_name=this.state.number;
                  SIPml.Stack.prototype.ao_sessions[key].o_session.o_stack.identity.s_user_name='99'+this.state.number;
                  SIPml.Stack.prototype.ao_sessions[key].o_session.o_stack.identity.s_impi='99'+this.state.number;
                  SIPml.Stack.prototype.ao_sessions[key].o_session.o_stack.identity.s_display_name=this.state.number;
              }else{
                  console.log("DEFOULT DATA FOR SIPML5");
              }
          }
      }

      callSession.call(this.state.counterparty.number);
      this.props.createSession(callSession);
      this.props.getContactInfo(this.state.counterparty.number);
      this.props.setCalledNumbers(currentState.state.agentNumber, this.state.number, this.state.counterparty.number);
      this.props.changeWindow('callingBoxOut');

  }

  chooseNumber(number, choosed, ownerid){
    this.setState({
      number: number,
      choosedNumber: choosed,
      ownerid: ownerid
    })
  }
  onNumberChange(e){
    this.setState({
      filterNumber: e.target.value,
      choosedNumber: null
    });
  }

  chooseCounterparty(counterparty, choosed){
    this.setState({
      counterparty: counterparty,
      choosedCounterparty: choosed,
      counterpartyid: counterparty.id
    });
  }

  onCounterpartyChange(e){
    this.setState({
      counterparty: e.target.value,
      filterCounterparty: e.target.value,
      choosedCounterparty: null
    });
  }


  render(){
    if(this.props.agentNumbers){
      return(
        <div className='calling-page'>
          <audio id="audio_remote" loop src="./sounds/ringbacktone.wav"> </audio>
          <div className="calling-header">
            {this.state.ownerid != null ?
              <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+this.state.ownerid}  target="_blank" >Agent page</a>
            :
              <a href="" className="blocked" >Agent page</a>
            }
            {this.state.counterpartyid != null ?
              <a href={"https://www.interbooker.com/admin/contact.php?IdContacts="+this.state.counterpartyid} target="_blank">
                {this.state.counterparty.firstName ? this.state.counterparty.firstName + ' ' : ''}
                {this.state.counterparty.middleName ? this.state.counterparty.middleName + ' ' : ''}
                {this.state.counterparty.lastName ? this.state.counterparty.lastName : ''}
              </a>
            :
              <a href="" className="blocked" >Customer page</a>
            }
            {this.state.choosedNumber == null || (this.state.choosedCounterparty == null && !this.state.counterparty) ?
              <button onClick={() => this.calling()} className="call-btn blocked" disabled><i className="fas fa-phone"></i></button>
            :
              <button onClick={() => this.calling()} className="call-btn"><i className="fas fa-phone"></i></button>
            }
            <a href="chrome-extension://pnppncpicheofnlphakbbkibbfkfkjnk/background.html" target="_blank">Microphone</a>
          </div>
          <div className="agent-numbers-wraper">
            <input onChange={(e) => this.onNumberChange(e)} placeholder="My Number" />
            <div className="agent-numbers">
              {_.map(this.props.agentNumbers, (number, i) => {
                if(this.state.filterNumber == '' || number.phonenumber.indexOf(this.state.filterNumber) != -1){
                  return(
                    <div key={i} className={this.state.choosedNumber == i ? "choosed agent-number" : "agent-number"} onClick={() => this.chooseNumber(number.phonenumber, i, number.ownerid)}>
                      {number.name}<br />
                      {number.phonenumber}
                    </div>
                  );
                }
              })}
            </div>
          </div>
          <div className="calling-numbers-wraper">
            <input onChange={(e) => this.onCounterpartyChange(e)} placeholder="Colling Number" />
            <div className="calling-numbers">
              {_.map(this.props.agentNumbers, (number, i) => {
                let isHeader = false;
                _.each(number.counterparties, (counterparty, j) => {
                  if(this.state.filterCounterparty == '' || counterparty.number.indexOf(this.state.filterCounterparty) != -1){
                    isHeader = true;
                    return false;
                 }
                })
                if(isHeader){
                  return(
                      <div className="calling-number-block" key={i}>
                        {number.name}
                        {_.map(number.counterparties, (counterparty, j) => {
                          if(this.state.filterCounterparty == '' || counterparty.number.indexOf(this.state.filterCounterparty) != -1){
                            return(
                                <div key={j} className={this.state.choosedCounterparty == i+'.'+j ? "choosed calling-number" : "calling-number"}
                                 onClick={() => this.chooseCounterparty(counterparty, i+'.'+j)}>
                                  {counterparty.number}
                                </div>
                            );
                          }
                        })}
                      </div>
                    )
                  }
              })}
            </div>
          </div>
        </div>
      );
    } else {
      return(<img src="assets/img/loader.gif" style={{display: 'block', marginLeft: 'auto', marginRight: 'auto'}}/>);
    }
  }
}

function mapStateToProps(state){
    return{
      agentNumbers: state.numbersReducer.numbers
    }
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({
    loadAgent: loadAgent,
    getNumbers: getNumbers,
    changeWindow: changeWindow,
    createSession: createSession,
    setCalledNumbers: setCalledNumbers,
    getContactInfo: getContactInfo
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCall);
