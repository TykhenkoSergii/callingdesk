import {LOAD_AGENT} from '../actions/';

const defaultWindow = {
  agent: ''
}

export default (state = defaultWindow, action) => {
  switch(action.type){
    case LOAD_AGENT:
      console.log(action)
      return{agent: action.payload}
      break;
    default:
      return{...state}
  }
}
