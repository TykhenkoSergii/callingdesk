import {ALL_CONTACTS} from '../actions/';

const defaultContacts = {
  contacts: null
};

export default (state = defaultContacts, action) => {
  switch(action.type){
    case ALL_CONTACTS:
      return{...state, contacts: action.payload}
      break;
    default:
      return{...state}
  }
}
