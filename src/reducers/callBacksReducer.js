import {LOAD_CALLBACKS, CALLBACK_CHANGED} from '../actions/';

const defaultContacts = null;

export default (state = defaultContacts, action) => {
  switch(action.type){
    case LOAD_CALLBACKS:
      return {...action.payload}
      break;

    case CALLBACK_CHANGED:
      return {...state, [action.payload.id + 'string']: {...state[action.payload.id + 'string'], ...action.payload}};

      break;

    default:
      return {...state}
  }
}
