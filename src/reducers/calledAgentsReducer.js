import {CALLED_AGENTS} from '../actions/';

const defaultWindow = [];

export default (state = defaultWindow, action) => {
  switch(action.type){
    case CALLED_AGENTS:
      return{...state, ...action.payload}
      break;
    default:
      return{...state}
  }
}
