import {CALLED_NUMBER, CLEAR_CALLED_NUMBER} from '../actions/';

const defaultNumber = {
  counterparty: null,
  identity: null,
  direction: null
};

export default (state = defaultNumber, action) => {
  switch(action.type){
    case CALLED_NUMBER:
      if(action.payload.identity && action.payload.identity.indexOf('99') == 0){
        action.payload.identity = action.payload.identity.substr(2);
      }
      return {...action.payload}
      break;
    case CLEAR_CALLED_NUMBER:
      return {...defaultNumber}

    default:
      return {...state}
  }
}
