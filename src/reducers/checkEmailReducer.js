import {CHECK_EMAIL} from '../actions/';

const defaultResult = {};

export default (state = defaultResult, action) => {
  switch(action.type){
    case CHECK_EMAIL:
      return{...action.payload}
      break;
    default:
      return{...state}
  }
}
