import {LOAD_CONFERENCE_PARTICIPANTS} from '../actions/';

const defaultPart = {};

export default (state = defaultPart, action) => {
  switch(action.type){
    case LOAD_CONFERENCE_PARTICIPANTS:
      return{...action.payload}
      break;
    default:
      return{...state}
  }
}
