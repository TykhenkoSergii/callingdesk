import {ACCOUNT_INFO, CALLING_INNER, ON_CHOOSING_COUNTERPARTY, SAVED_BOOKING, NEW_ACCOUNT_INFO} from '../actions/';

const defaultWindow = {
  id: false
};

export default (state = defaultWindow, action) => {
  switch(action.type){
    case ACCOUNT_INFO:
      const identity =  action.payload.number < 999999999 ? 'inner' :
                        action.payload.last_call_identity ? action.payload.last_call_identity :
                        action.payload.current_booking_identity ? action.payload.current_booking_identity :
                        action.payload.future_booking_identity ? action.payload.future_booking_identity :
                        action.payload.past_booking_identity ? action.payload.past_booking_identity : null;
                        
      return{...action.payload, identity}
      break;

    case CALLING_INNER:
      let inner = {
        ...action.payload.response,
        id: 'inner',
        firstName: action.payload.innerName
      }
      return inner;
      break;

    case ON_CHOOSING_COUNTERPARTY:
      return {...state, identity: action.payload}
      break;

    case SAVED_BOOKING:

      return {...state, firstName: action.payload.response.displayname, id: action.payload.response.ownerid, booking_id: action.payload.booking_id, guid: action.payload.response.owner}
      break;

    case NEW_ACCOUNT_INFO:
      return {...state, identity: null, guid: action.payload.counterparty_guid, booking_id: action.payload.booking_id, img:"https://www.interbooker.com/admin/css/no-user.png", id: action.payload.counterparty_id, firstName: action.payload.counterparty_name, number: action.payload.counterparty_number}
      break;

    default:
      return{...state}
  }
}
