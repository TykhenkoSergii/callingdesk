import {COUNTERPARTY_ACCOUNT, NEW_COUNTERPARTY_INFO} from '../actions/';

const defaultWindow = {
  id: false
};

export default (state = defaultWindow, action) => {

  switch(action.type){
    case COUNTERPARTY_ACCOUNT:
    
      return{...action.payload}
      break;

    case NEW_COUNTERPARTY_INFO:
      return {...state, guid: action.payload.counterparty_guid, booking_id: action.payload.booking_id, img:"https://www.interbooker.com/admin/css/no-user.png", id: action.payload.counterparty_id, firstName: action.payload.counterparty_name, number: action.payload.counterparty_number}
      break;

    default:
      return{...state}
  }
}
