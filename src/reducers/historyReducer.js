import {LOAD_HISTORY, HISTORY_CHANGED} from '../actions/';

const defaultHistory = {
  history: null,
  offset: 0
};

export default (state = defaultHistory, action) => {

  switch(action.type){
    case LOAD_HISTORY:
    
      const offset = action.payload.offset || 0;
      const counterparty = action.payload.counterparty || null;
      const history = state.counterparty == action.payload.counterparty ? {...state.history, ...action.payload.history} : {...action.payload.history};
      const end = action.payload.history.end ? action.payload.history.end : false;
      return {...state, history, offset, counterparty, end}
      break;

    case HISTORY_CHANGED:
      const histories = {...state};
      action.payload.forEach(history => {
        histories.history[history.id].contactId = history.counterparty_id;
        histories.history[history.id].contactGuid = history.counterparty_guid;
        histories.history[history.id].contact = history.counterparty_name;
      });
      return {...histories};
      break;

    default:
      return {...state}
  }
}
