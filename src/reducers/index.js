import {combineReducers} from 'redux';
import windowReducer from './windowReducer';
import agentReducer from './agentReducer';
import historyReducer from './historyReducer';
import sipSessionReducer  from './sipSessionReducer';
import calledNumberReducer from './calledNumberReducer';
import contactInfoReducer from './contactInfoReducer';
import microphoneErrorReducer from './microphoneErrorReducer';
import calledAgentsReducer from './calledAgentsReducer';
import allContactsReducer from './allContactsReducer';
import voipQReducer from './voipQReducer';
import soundStatusReducer from './soundStatusReducer';
import onlineAgentsReducer from './onlineAgentsReducer';
import checkEmailReducer from './checkEmailReducer';
import callBacksReducer from './callBacksReducer';
import conferenceParticipants from './conferenceParticipants';
import counterpartyReducer from './counterpartyReducer';

export default combineReducers({
  windowReducer,
  agentReducer,
  historyReducer,
  sipSessionReducer,
  calledNumberReducer,
  contactInfoReducer,
  counterpartyReducer,
  microphoneErrorReducer,
  calledAgentsReducer,
  allContactsReducer,
  voipQReducer,
  soundStatusReducer,
  onlineAgentsReducer,
  checkEmailReducer,
  callBacksReducer,
  conferenceParticipants,
});
