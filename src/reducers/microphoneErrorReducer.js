import {MICROPHONE_ERROR} from '../actions/';

const defaultError = {
  error: null
};

export default (state = defaultError, action) => {
  switch(action.type){
    case MICROPHONE_ERROR:
      return {...state, error: action.payload}
      break;
    default:
      return {...state}
  }
}
