import {ONLINE_AGENTS} from '../actions/';

const defaultContacts = [];

export default (state = defaultContacts, action) => {
  switch(action.type){
    case ONLINE_AGENTS:
      return{...action.payload}
      break;
    default:
      return{...state}
  }
}
