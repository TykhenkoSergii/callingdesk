import {CREATE_SESSION} from '../actions/';

const defaultSession = null;

export default (state = defaultSession, action) => {
  switch(action.type){
    case CREATE_SESSION:
      return {...state, session: action.payload}
      break;
    default:
      return {...state}
  }
}
