import {SOUND_STATUS} from '../actions/';

export default (state = true, action) => {
  switch(action.type){
    case SOUND_STATUS:
      return action.payload;

      break;
    default:
      return state;
  }
}
