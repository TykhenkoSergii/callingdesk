import {CHANGE_WINDOW} from '../actions/';

const defaultWindow = {
  window: 'calling',
  status: ''
}

export default (state = defaultWindow, action) => {
  switch(action.type){
    case CHANGE_WINDOW:
      return{...state, window: action.payload.currentWindow, status: action.payload.status}
      break;
    default:
      return{...state}
  }
}
